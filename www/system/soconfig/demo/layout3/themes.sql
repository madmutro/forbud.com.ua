
INSERT INTO {table_prefix}layout_module VALUES
(null,{home_layout_id},"so_html_content.241","footer_block1","0"),
(null,{home_layout_id},"so_newletter_custom_popup.234","footertop","2"),
(null,{home_layout_id},"so_newletter_custom_popup.202","footertop","1"),
(null,{home_layout_id},"so_newletter_custom_popup.128","footertop","0"),
(null,{home_layout_id},"so_tools.245","content_top","2"),
(null,{home_layout_id},"so_quickview.228","content_top","1"),
(null,{home_layout_id},"so_page_builder.239","content_top","0"),
(null,{home_layout_id},"so_searchpro.235","content_search","2"),
(null,{home_layout_id},"so_searchpro.231","content_search","1"),
(null,{home_layout_id},"so_searchpro.94","content_search","0"),
(null,{home_layout_id},"so_megamenu.168","content_menu1","0"),
(null,{home_layout_id},"so_megamenu.115","content_menu","1"),
(null,{home_layout_id},"so_html_content.177","footer_block2","0"),
(null,{home_layout_id},"so_html_content.232","footer_block3","0"),
(null,{home_layout_id},"so_html_content.182","footer_block4","0"),
(null,{home_layout_id},"so_popular_tags.166","footerbottom","0");



INSERT INTO {table_prefix}soconfig VALUES
("25939","0",'soconfig_general_store', '{\"typelayout\":\"3\",\"themecolor\":\"blue\",\"layouts\":\"1\",\"backtop\":\"1\",\"scroll_animation\":\"0\",\"copyright\":\"SO ShoppyStore \\u00a9  2015 -  {year}. Opencart Themes Demo Store. All Rights Reserved. Designed by MagenTech.Com\",\"typeheader\":\"3\",\"toppanel_status\":\"0\",\"toppanel_type\":\"1\",\"phone_status\":\"1\",\"contact_number\":{\"2\":\"0123 456 7891\",\"1\":\"0123 456 7891\"},\"welcome_message_status\":\"1\",\"welcome_message\":{\"2\":\"&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;\\/i&gt; Email: Contact@domain.com\",\"1\":\"&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;\\/i&gt; Email: Contact@domain.com\"},\"wishlist_status\":\"1\",\"checkout_status\":\"0\",\"lang_status\":\"1\",\"typefooter\":\"1\",\"preloader\":\"0\",\"imgpreloader\":\"\",\"imgpayment_status\":\"0\",\"imgpayment\":\"\",\"type_banner\":\"11\",\"contentbg\":\"\"}', 1),
("25944","0","soconfig_advanced_store","{\"name_color\":\"cyan\",\"theme_color\":\"#df1f25\",\"scsscompile\":\"0\",\"scssformat\":\"Expanded\",\"compileMutiColor\":\"0\",\"minify_css\":\"0\",\"minify_js\":\"0\"}","1"),
("25949","0","soconfig_layout_store","{\"layoutstyle\":\"full\",\"general_bgcolor\":\"\",\"pattern\":\"none\",\"content_bg_mode\":\"repeat\",\"content_attachment\":\"scroll\"}","1"),
("25954","0","soconfig_pages_store","{\"product_catalog_refine\":\"2\",\"product_catalog_refine_col_lg\":\"6\",\"product_catalog_refine_col_md\":\"4\",\"product_catalog_refine_col_sm\":\"3\",\"product_catalog_refine_col_xs\":\"2\",\"deals_today\":\"0\",\"lstimg_cate_status\":\"0\",\"product_catalog_mode\":\"0\",\"product_catalog_column_lg\":\"5\",\"product_catalog_column_md\":\"3\",\"product_catalog_column_sm\":\"2\",\"product_catalog_column_xs\":\"1\",\"other_catalog_column_lg\":\"4\",\"other_catalog_column_md\":\"3\",\"other_catalog_column_sm\":\"2\",\"other_catalog_column_xs\":\"1\",\"secondimg\":\"2\",\"rating_status\":\"1\",\"lstdescription_status\":\"0\",\"sale_status\":\"1\",\"sale_text\":{\"2\":\"\",\"1\":\"\"},\"new_status\":\"0\",\"new_text\":{\"2\":\"\",\"1\":\"\"},\"days\":\"\",\"quick_status\":\"1\",\"quick_view_text\":{\"2\":\"Quick view\",\"1\":\"Quick view\"},\"discount_status\":\"1\",\"countdown_status\":\"1\",\"radio_style\":\"0\",\"check_style\":\"0\",\"thumbnails_position\":\"bottom\",\"product_enablezoom\":\"1\",\"product_zoommode\":\"inner\",\"product_zoomlenssize\":\"350\",\"tabs_position\":\"1\",\"product_page_button\":\"0\",\"product_socialshare\":{\"2\":\"\",\"1\":\"\"},\"related_status\":\"1\",\"related_position\":\"horizontal\",\"product_related_column_lg\":\"4\",\"product_related_column_md\":\"2\",\"product_related_column_sm\":\"2\",\"product_related_column_xs\":\"1\"}","1"),
("25959","0","soconfig_fonts_store","{\"body_status\":\"google\",\"normal_body\":\"inherit\",\"url_body\":\"https:\\/\\/fonts.googleapis.com\\/css?family=Roboto:400,500,700,300\",\"family_body\":\"Roboto, sans-serif\",\"selector_body\":\"body\",\"menu_status\":\"standard\",\"normal_menu\":\"inherit\",\"url_menu\":\"\",\"family_menu\":\"\",\"selector_menu\":\"\",\"heading_status\":\"standard\",\"normal_heading\":\"inherit\",\"url_heading\":\"\",\"family_heading\":\"\",\"selector_heading\":\"\"}","1"),
("25964","0","soconfig_social_store","{\"social_fb_status\":\"0\",\"facebook\":\"\",\"social_twitter_status\":\"0\",\"twitter\":\"\",\"social_custom_status\":\"0\",\"video_code\":{\"2\":\"\",\"1\":\"\"}}","1"),
("25969","0","soconfig_custom_store","{\"cssinput_status\":\"0\",\"custom_css\":\"\",\"cssfile_status\":\"0\",\"cssfile\":[\"catalog\\/view\\/theme\\/so-hurama\\/css\\/custom.css\"],\"jsinput_status\":\"0\",\"custom_js\":\"\",\"jsfile_status\":\"0\",\"jsfile\":[\"catalog\\/view\\/theme\\/so-hurama\\/js\\/custom.js\"]}","1");