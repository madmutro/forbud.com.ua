            <?php $setting_field = 'divide_by_options'; ?>
            <tr>
                <td><?php echo ${'text_setting_'.$setting_field}; ?></td>
                <td>
                    
                    <h4 style="margin-bottom: 6px; margin-top: 9px; padding-left: 7px;">Использовать единый артикул в таблице значений продуктых опций (выбирите название колонки в таблице product_option_value, в которой расположен артикул значения продуктовой опции)</h4>
                    
                    <div class="scrollbox" style="padding: 7px; background: white !important">
                            <select name="setting[<?php echo $setting_field; ?>][option_value_sku]"  class="form-control" style="margin-bottom: 5px;">
                                    <option value="0" >Выбрать единый артикул продуктовой опции</option>
                                        <?php foreach($product_option_value_columns as $product_field => $product_value){ ?>
                                            <?php if(isset($setting[$setting_field]['option_value_sku']) && $setting[$setting_field]['option_value_sku']==$product_field ){ ?>
                                    <option value="<?php echo $product_field ?>" selected="" ><?php echo $product_value; ?></option>
                                            <?php }else{ ?>
                                    <option value="<?php echo $product_field ?>" ><?php echo $product_value; ?></option> 
                                            <?php } ?>
                                        <?php } ?>
                                </select>
                    </div>
                    
                    
                    <?php for($o=0;$o<5;$o++){ ?>
                    
                    
                    
                    <h4 style="margin-bottom: 6px; margin-top: 9px; padding-left: 7px;">Опция <?php echo ($o+1); ?></h4>
                    
                    <div class="scrollbox" style="padding: 7px; background: white !important">
                        
                    <?php $setting_field = 'divide_on_options_selected'; ?>
                    
                    <select id="setting_fields<?php echo $setting_id ?>divide_on_options_option_id<?php echo $o; ?>_sel" class="form-control" name="setting[<?php echo $setting_field; ?>][<?php echo $o; ?>]" onchange="if(this.value!=0){ $('#setting_fields<?php echo $setting_id ?>divide_on_options_option_id<?php echo $o; ?>').show() }else{ $('#setting_fields<?php echo $setting_id ?>divide_on_options_option_id<?php echo $o; ?>').hide() }">
                        <?php if(isset($setting[$setting_field][$o]) && $setting[$setting_field][$o]){ ?>
                            <option selected="" value="<?php echo $setting_field; ?>">Выбрать опцию</option>
                            <option value="0">Не использовать опцию</option>
                        <?php }else{ ?>
                            <option value="<?php echo $setting_field; ?>">Выбрать опцию</option>
                            <option selected="" value="0">Не использовать опцию</option>
                        <?php } ?>
                    </select>
                    
                    <div id="setting_fields<?php echo $setting_id; ?>divide_on_options_option_id<?php echo $o; ?>" style="margin-top:5px;<?php if(isset($setting[$setting_field]) && !$setting[$setting_field] || !isset($setting[$setting_field])){ ?> display: none; <?php } ?>">
                        
                        
                        
                        <div class="setting_divide_on_options_option_id<?php echo $o; ?><?php echo $setting_id ?>"></div>
                        <script type="text/javascript"><!--

                            $(document).ready(function() {
                                getSettingFieldsYamarket(<?php echo $sample_setting_id; ?>,'<?php echo $setting_type ?>',<?php echo $setting_id ?>,'divide_on_options_option_id<?php echo $o; ?>');
                                $('#setting_fields<?php echo $setting_id ?>divide_on_options_option_id<?php echo $o; ?>_sel').change();
                            });

                    //--></script>
                        
                        
                        
                        
                        <table class="table table-bordered table-hover">
                            <?php $setting_field = 'divide_on_options_prefix'; ?>
                            <tr>
                                <td><?php echo ${'text_setting_'.$setting_field}; ?></td>
                                <td>
                                    <input  type="text" class="form-control" value="<?php if(isset($setting[$setting_field][$o]) && $setting[$setting_field][$o]!==''){ echo $setting[$setting_field][$o]; }else{ ?>OPTION<?php echo $o; ?>_<?php } ?>" name="setting[<?php echo $setting_field; ?>][<?php echo $o; ?>]" />
                                </td>
                            </tr>
                            <?php $setting_field = 'divide_on_options_available_by_option_quantity'; ?>
                            <tr>
                                <td><?php echo ${'text_setting_'.$setting_field}; ?></td>
                                <td>
                                    <select  class="form-control" name="setting[<?php echo $setting_field; ?>][<?php echo $o; ?>]">
                                        <?php if(isset($setting[$setting_field][$o]) && $setting[$setting_field][$o] == $setting_field){ ?>
                                            <option selected="" value="<?php echo $setting_field; ?>"><?php echo $text_enable; ?></option>
                                            <option value="0"><?php echo $text_disable; ?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $setting_field; ?>"><?php echo $text_enable; ?></option>
                                            <option selected="" value="0"><?php echo $text_disable; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <?php $setting_field = 'divide_on_options_add_to_model'; ?>
                            <tr>
                                <td><?php echo ${'text_setting_'.$setting_field}; ?></td>
                                <td>
                                    <select  class="form-control" name="setting[<?php echo $setting_field; ?>][<?php echo $o; ?>]">
                                        <?php if(isset($setting[$setting_field][$o]) && $setting[$setting_field][$o] == $setting_field){ ?>
                                            <option selected="" value="<?php echo $setting_field; ?>"><?php echo $text_enable; ?></option>
                                            <option value="0"><?php echo $text_disable; ?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $setting_field; ?>"><?php echo $text_enable; ?></option>
                                            <option selected="" value="0"><?php echo $text_disable; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            <?php $setting_field = 'divide_on_options_add_to_name'; ?>
                            <tr>
                                <td><?php echo ${'text_setting_'.$setting_field}; ?></td>
                                <td>
                                    <select  class="form-control" name="setting[<?php echo $setting_field; ?>][<?php echo $o; ?>]">
                                        <?php if(isset($setting[$setting_field][$o]) && $setting[$setting_field][$o] == $setting_field){ ?>
                                            <option selected="" value="<?php echo $setting_field; ?>"><?php echo $text_enable; ?></option>
                                            <option value="0"><?php echo $text_disable; ?></option>
                                        <?php }else{ ?>
                                            <option value="<?php echo $setting_field; ?>"><?php echo $text_enable; ?></option>
                                            <option selected="" value="0"><?php echo $text_disable; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>
                            
                            
                        </table>
                        
                        
                        
                        
                        
                    </div>
                    
                    </div>
                                
                    <?php } ?>
                </td>
            </tr>