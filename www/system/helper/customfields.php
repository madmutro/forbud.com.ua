<?php 
	// return array with all fields
	function get_fields () {
		global $fields;
		return $fields;
	}
	// return value of field or ''
	function get_field_value ($name) {
		global $fields;
		$result = '';
		foreach ($fields as $field) {
			if ($field['name'] == $name) $result = $field['value'];
		}
		return htmlspecialchars_decode($result);
	}
?>