<?php
class CustomFields extends Controller {
    public function getFields() {
        global $fields;
        $fields = array();
        $this->load->model('extension/module/customfields');
        $fields = $this->model_extension_module_customfields->getAllFields();
        $this->load->helper('customfields');
    }
}