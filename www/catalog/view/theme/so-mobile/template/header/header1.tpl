<?php 
//Check Home page
$barStatic = $mobile['barnav'] ? '' : 'bar-static';
if($this->soconfig->is_mobile_page() || $this->soconfig->is_home_page()) : 
?>
	<header class=" bar bar-nav bar-navhome typeheader-<?php echo isset($mobile['mobileheader']) ? $mobile['mobileheader'] : '0'?> <?php echo $barStatic;?>">
		<div class="row navbar-bar ">
			<!-- LOGO -->
			<div class="navbar-menu col-xs-2">
			   <a class="toggle-panel" href="#panel-menu">
			   	<span class="icon-bar"></span>
			   	<span class="icon-bar bar2"></span>
			   	<span class="icon-bar"></span>
			   </a>
			</div>
			<div class="navbar-logo col-xs-3">
			   <?php  $this->soconfig->get_logoMobile();?> 
			</div>
			<div class="navbar-search col-xs-7">
				<!-- BOX CONTENT SEARCH -->
				<?php echo $search; ?>
			</div>
		</div>
	</header>
<?php 
//Check Subpage page
else: ?>
	<header class="bar bar-nav bar-modify <?php echo $barStatic;?>">
		<a class="btn btn-link btn-nav pull-left" href="#" onClick="history.go(-1); return false;">
			<span class="icon icon-left-nav"></span>
		</a>
		<a class="btn btn-link btn-nav pull-right toggle-panel" href="#panel-menu">
			<span class="icon icon-bars"></span>
		</a>
		<h1 class="title"><?php echo $title ?></h1>
		<div class="row">
			<div class="col-xs-12">
				<div class="form-group contact-info">
					<a href="tel:+380992646437">+38 (099) 264-64-37</a>
					<a href="viber://chat?number=%2B380992646437&text=Интересует вопрос по <?php echo $title ?>" class="viber-href"><img src="/image/catalog/logotypes/viber.svg" alt=""> <span>Viber</span> </a>
					<a href="tg://resolve?domain=forbud" class="telegram-href"><img src="/image/catalog/logotypes/telegram.svg" alt=""> <span>Telegram</span> </a>
				</div>
			</div>
		</div>
	</header>
<?php endif;?>