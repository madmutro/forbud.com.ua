<?php if($type) {?>
<div class="row categories_sidebar<?php echo $module; ?>" id="categorymenu_list">
    <div class="col-12">
        <div class="menulist-title">
            <span><?php echo $heading_title; ?></span>
        </div>
        <ul class="menulist-content">
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<?php } ?>