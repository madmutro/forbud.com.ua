<?php if ($callback_active == 1) { ?>
<div class="modal fade" id="modalFeedbackHeader" tabindex="-1" role="dialog" aria-labelledby="modalFeedbackHeaderLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div style="display:inline-block; width: 100%; text-align:right;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="form-horizontal" id="form-feedback-header">
                    <div class="form-group required">
                        <div class="col-sm-12">
                            <label class="control-label" for="input-name-callback"><?php echo $entry_name; ?></label>
                            <input type="text" name="name" value="<?php echo $name_callback; ?>" id="input-name-callback" class="form-control" />
                        </div>
                        <div class="col-sm-12">
                            <label class="control-label" for="input-phone-callback"><?php echo $entry_phone; ?></label>
                            <input type="text" name="phone" value="<?php echo $phone_callback; ?>" id="input-phone-callback" class="form-control" />
                        </div>
                    </div>
                </form>
                <button type="button" id="button_send_feedback_header" data-loading-text="<?php echo $text_loading; ?>"  class="btn btn-primary"><?php echo $text_send; ?></button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#button_send_feedback_header').on('click', function () {
        $.ajax({
            url: 'index.php?route=common/header/write',
            type: 'post',
            dataType: 'json',
            data: $("#form-feedback-header").serialize(),
            beforeSend: function () {
                $('#button_send_feedback_header').button('loading');
            },
            complete: function () {
                $('#button_send_feedback_header').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();
                if (json['error']) {
                    $('#form-feedback-header').after('<div class="alert alert-danger" style="margin:20px 0;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }
                if (json['success']) {
                    $('#form-feedback-header').after('<div class="alert alert-success" style="margin:20px 0;"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                    $('input[name=\'name\']').val('');
                    $('input[name=\'phone\']').val('');
                }
            }
        });
    });
</script>
<?php } ?>


<header id="header" class=" variant typeheader-<?php echo isset($typeheader) ? $typeheader : '1'?>">
    <!-- HEADER TOP -->
    <div class="header-top compact-hidden">
        <div class="container">
            <div class="header-top-left  col-sm-4 hidden-xs">
                <!--<ul class="list-inline">
						<?php if($phone_status):?>
						<li class="hidden-xs hidden-sm" >
							<?php
								if (isset($contact_number) && is_string($contact_number)) {
									echo html_entity_decode($contact_number, ENT_QUOTES, 'UTF-8');
								} else {echo 'Telephone No';}
							?>
						</li>
						<?php endif; ?>
						<?php if($welcome_message_status):?>
						<li class="hidden-xs" >
							<?php
								if (isset($welcome_message) && is_string($welcome_message)) {
									echo html_entity_decode($welcome_message, ENT_QUOTES, 'UTF-8');
								} else {echo 'Default welcome msg!';}
							?>
						</li>
						<?php endif; ?>
					</ul>-->
                <div class="txt"><?=$text_store_legend_top?></div>
            </div>
            <div class="header-top-right collapsed-block col-sm-8 text-right">
                <div class="icons">
                    <a href="https://www.facebook.com/forbud.shop" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="https://www.instagram.com/_forbud_/" target="_blank"><i class="fa fa-instagram"></i></a>
                </div>

                <div class="btn-group tabBlocks">
                    <ul class="top-link list-inline">
                        <!-- WISHLIST  -->
                        <?php if($wishlist_status):?>
                        <li class="wishlist"><a href="<?php echo $wishlist; ?>" id="wishlist-total" title="<?php echo $text_wishlist; ?>"><span><?php echo $text_wishlist; ?></span></a></li><?php endif; ?>
                        <li class="account" id="my_account"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="btn btn-link dropdown-toggle" data-toggle="dropdown"> <span><?php echo $text_account; ?></span> <span class="fa fa-angle-down"></span></a>
                            <ul class="dropdown-menu ">

                                <?php if ($logged) { ?>
                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                                <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                                <li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
                                <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                                <?php } else { ?>
                                <li><a href="<?php echo $register; ?>"><i class="fa fa-user"></i> <?php echo $text_register; ?></a></li>
                                <li><a href="<?php echo $login; ?>"><i class="fa fa-pencil-square-o"></i> <?php echo $text_login; ?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <!-- COMPARE -->
                        <!--<?php if($checkout_status):?><li class="checkout"><a href="<?php echo $checkout; ?>" class="btn btn-link" title="<?php echo $text_checkout; ?>"><span ><?php echo $text_checkout; ?></span></a></li><?php endif; ?>-->
                        <!-- LANGUAGE CURENTY -->
                        <?php if($lang_status):?>
                        <li><?php echo $currency; ?></li>
                        <li><?php echo $language; ?></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- HEADER CENTER -->
    <div class="header-center">
        <div class="container">
            <div class="nav-top">
                <!-- LOGO -->
                <div class="navbar-logo">
                    <?php  $this->soconfig->get_logo();?>
                    <div class="subtitle"><?=$text_logo_subtitle_top?></div>
                </div>
                <div class="mega-horizontal">
                    <div class="phones">
                        <div class="txt"><a href="tel:+380639412332">+38 (063) 941-23-32</a></div>
                        <div class="txt"><a href="tel:+380992646437">+38 (099) 264-64-37</a></div>
                        <div class="txt"><a href="tel:+380972626234">+38 (097) 262-62-34</a></div>
                        <div class="txt messangers">
                            <a href="viber://chat?number=%2B380992646437&text=Интересует вопрос по <?php echo $title ?>" class="viber-href"><img src="/image/catalog/logotypes/viber.svg" alt=""> <span>Viber</span> </a>
                            <a href="tg://resolve?domain=forbud" class="telegram-href"><img src="/image/catalog/logotypes/telegram.svg" alt=""> <span>Telegram</span> </a>
                        </div>
                    </div>
                    <div class="text-box">
                        <div class="item"><span class="top"><?=$text_email_top?></span>
                            <div class="txt"><a href="mailto:info.forbud@gmail.com">info.forbud@gmail.com</a></div>
                        </div>
                        <?php if ($callback_active == 1) { ?>
                        <div class="item"><span><?=$text_recall_top?></span>
                            <div class="txt"><a href="#modalFeedbackHeader" id="button_feedback" data-toggle="modal"><i class="fa fa-phone"></i> <?php echo $text_button_callback; ?></a></div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="text-box">
                        <div class="item">
                            <?=$text_work_time_top?>
                        </div>
                    </div>
                </div>
                <div class="shopping_cart pull-right">
                    <!-- BOX CART -->
                    <?php echo $cart; ?>
                </div>
            </div>
            <div class="nav-links">
                <!-- BOX CONTENT SEARCH -->
                <?php echo $content_menu; ?>
            </div>
        </div>
    </div>

    <!-- HEADER BOTTOM -->
    <div class="header-bottom">
        <div class="container">
            <div class="row">

                <div class="col-sm-3 col-xs-4 menu-vertical">
                    <?php echo $content_menu1; ?>
                </div>

                <div class="col-sm-9 col-xs-8 content_menu">
                    <?php echo $content_search; ?>
                </div>

            </div>
        </div>

    </div>

    <!-- Navbar switcher -->
    <?php if (!isset($toppanel_status) || $toppanel_status != 0) : ?>
    <?php if (!isset($toppanel_type) || $toppanel_type != 2 ) :  ?>
    <div class="navbar-switcher-container">
        <div class="navbar-switcher">
            <span class="i-inactive">
                <i class="fa fa-caret-down"></i>
            </span>
            <span class="i-active fa fa-times"></span>
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>
</header>