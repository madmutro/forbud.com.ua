<?php if($type) { ?>
<div class="row categories-wall" >
    <?php foreach ($categories as $category) { ?>
        <div class="col-lg-3 col-md-4 col-sm-4 col-4 category-item">
            <div class="category-item__block">
                <div class="image"><a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" class="img-responsive" /></a></div>
                <div class="caption">
                    <h5><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h5>
                </div> 
            </div>
        </div>
    <?php } ?>
</div>
<?php } else { ?>
<div id="category_carousel<?php echo $module; ?>" class="owl-carousel">
  <?php foreach ($categories as $category) { ?>
  <div class="item text-center">
    <a href="<?php echo $category['href']; ?>"><img src="<?php echo $category['image']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" /></a>
    <h5><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></h5>
  </div>
  <?php } ?>
</div>
<script type="text/javascript"><!--
$('#category_carousel<?php echo $module; ?>').owlCarousel({
	items: 6,
	autoPlay: 3000,
	navigation: true,
	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>'],
	pagination: true
});
--></script>
<?php } ?>










<div class="container">



</div>





