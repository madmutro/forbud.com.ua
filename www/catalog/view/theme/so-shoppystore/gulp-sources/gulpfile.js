var gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  cleanCSS = require('gulp-clean-css');

gulp.task('style', function () {
  return gulp.src('custom-style.scss')
    .pipe(sass())
    .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
    .pipe(cleanCSS())
    .pipe(gulp.dest('../css'));
});

gulp.task('watch', function() {
  gulp.watch('custom-style.scss', gulp.series('style'));
});

gulp.task('default', gulp.series('watch', 'style'));



