<?php
class ControllerExtensionModuleCategories extends Controller {
    public function index($setting) {
		static $module = 0;
        $this->load->language('extension/module/categories');
		
		if (!$setting['type']) {
			$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
            $this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.theme.default.css');
			$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.transitions.css');
			$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');
		}

        $data['categories'] = array();
		$categories = array();
		$data['type'] = $setting['type'];
		
		$this->load->model('extension/module/categories');
		
		$language_id = $this->config->get('config_language_id');
		$title = html_entity_decode($setting['title'][$language_id], ENT_QUOTES, 'UTF-8');
		
		switch ($setting['show_categories']) {
			case 0:
				$categories = $this->model_extension_module_categories->getNewCategories($setting['limit']);
				$data['heading_title'] = $title ? $title : $this->language->get('heading_title_new');
				break;
			case 1:
				$list = '';
				if(isset($setting['category'])){
				foreach($setting['category'] as $category){
					$list .= $category . ',';
				}
				$categories = $this->model_extension_module_categories->getFeaturedCategories(substr($list, 0, -1), $setting['limit']);
				$data['heading_title'] = $title ? $title : $this->language->get('heading_title_featured');
				}
				break;
			case 2:
				$categories = $this->model_extension_module_categories->getRandomCategories($setting['limit']);
				$data['heading_title'] = $title ? $title : $this->language->get('heading_title_random');
				break;
		}
		
		if ($categories) {
			$this->load->model('tool/image');

			foreach ($categories as $category) {
				if ($category['image']) {
					$image = $this->model_tool_image->resize($category['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}
				
				$data['categories'][] = array(
					'name'  => $category['name'],
					'image' => $image,
					'href'  => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
			
			$data['module'] = $module++;


			if ($data['type'] == 2) {
                return $this->load->view('extension/module/categories_sidebar', $data);
            }
            else {
                return $this->load->view('extension/module/categories', $data);
            }
		}
    }
}