<?php

class ControllerExtensionModuleCustomfields extends Controller {

	public function index() {		
		global $fields;
		$fields = array();
		$this->load->model('extension/module/customfields');
		$fields = $this->model_extension_module_customfields->getAllFields();
		$this->load->helper('customfields');
	}	
}