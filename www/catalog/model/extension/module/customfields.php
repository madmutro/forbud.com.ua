<?php
class ModelExtensionModuleCustomfields extends Model {
	// function to get all fields
	public function getAllFields() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customfields_option");
		return $query->rows;
	}
	// function to write all fields
	public function writeAllFields($data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customfields_option");
		foreach ($data as $attr) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customfields_option SET type = '" . $this->db->escape($attr['type']) . "', name = '" . $this->db->escape($attr['name']) . "', description = '" . $this->db->escape($attr['description']) . "', value = '" . $this->db->escape($attr['value']) . "'");
		}
		return 'ok';
	}
}
?>