<?php
// Text
$_['google_image_width']    = '600';
$_['google_image_height']    = '600';

$_['pinterest_image_width']    = '600';
$_['pinterest_image_height']    = '900';

$_['twitter_image_width']    = '506';
$_['twitter_image_height']    = '253';

$_['facebook_image_width']    = '600';
$_['facebook_image_height']    = '315';

$_['mix_image_width']    = '600';
$_['mix_image_height']    = '600';

$_['twitter_imagesmall_width']    = '109';
$_['twitter_imagesmall_height']    = '82';