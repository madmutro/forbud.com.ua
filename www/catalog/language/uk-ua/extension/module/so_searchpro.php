<?php
// Text
$_['text_search']    	= 'Введіть тут ключові слова...';
$_['text_category_all'] = 'Всі категорії';
$_['text_tax']      	= 'Податки';
$_['text_price']      	= 'Ціна';
$_['button_cart']       = 'До кошика';
$_['button_wishlist']       = 'Додати в вибране';
$_['button_compare']       = 'Додати в порівняння';
