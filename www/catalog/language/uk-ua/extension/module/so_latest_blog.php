<?php
// Heading
$_['text_latest']  = 'Останні';
$_['text_comment']     = ' Коментар';
$_['text_comments']    = ' Коментарів';
$_['text_view']        = ' Дивитись все';
$_['text_views']       = ' Перегляди';
// Text
$_['text_tax']      = 'Додатково:';
$_['text_noitem']      = 'Немає елементів для показу!';
$_['text_no_database'] = 'Не встановлена База Даних для модуля, встановіть модуль "Simple Blog" зараз!';