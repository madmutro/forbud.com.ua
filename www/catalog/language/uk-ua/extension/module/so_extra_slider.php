<?php
// Heading
$_['heading_title'] = 'So Extra Слайдер';

// Text
$_['text_tax']      		= 'Додатково:';
$_['text_noproduct']      	= 'Немає продуктів для показу!';
$_['text_noitem']      	= 'Немає елементів для показу!';
$_['text_sale']      	= 'Знижка';
$_['text_new']      	= 'Новинки';