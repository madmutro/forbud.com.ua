<?php
// Heading
$_['heading_title'] 								= 'So Onepage Checkout';
$_['text_checkout_create_account_login']           	= 'Створити акаунт або увійти';
$_['text_confirm_order']           					= 'Підтвердити замовлення';
$_['text_shipping_address']           				= 'Адреса доставки';
$_['text_title_shipping_method']           			= 'Метод доставки';
$_['text_title_payment_method']           			= 'Метод оплати';
$_['text_coupon_voucher']           				= 'Ви маєте купон або ваучер?';
$_['text_enter_coupon_code']           				= 'Введіть код купону';
$_['text_enter_voucher_code']           			= 'Введіть код ваучеру';
$_['text_apply_voucher']           					= 'Використати ваучер';
$_['text_enter_reward_points']           			= 'Введіть бонусні бали';
$_['text_apply_points']           					= 'Використати бали';
$_['text_shopping_cart']           					= 'Сторінка замовлення';
$_['text_payment_detail']          					= 'Деталі оплати';

//Error
$_['error_comment']									= 'Ви повинні додати коментар до замовлення.';