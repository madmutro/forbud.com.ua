<?php
// Heading
$_['heading_title'] = 'So Фільтр';

// Text
$_['text_tax']      		= 'Додатково:';
$_['text_noproduct']      	= 'немає продуктів для показу!';
$_['text_sale']      		= 'Знижка';
$_['text_new']      		= 'Новинка';

$_['text_search']      		= 'Пошук';
$_['text_price']      		= 'Ціна';
$_['text_reset_all']      	= 'Очистити';
$_['text_manufacturer']     = 'Виробник';
$_['text_subcategory']     	= 'Підкатегорія';