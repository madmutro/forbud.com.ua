<?php
// Heading
$_['heading_title'] = 'So знижки';

// Text
$_['text_tax']      		= 'Додатково:';
$_['text_noitem']      		= 'Немає елементів для показа!';
$_['text_sale']      		= 'Знижка';
$_['text_new']      		= 'Новинка';

$_['text_Day']      		= 'День';
$_['text_Hour']      		= 'Година';
$_['text_Min']      		= 'Хвилина';
$_['text_Sec']      		= 'Секунда';
$_['text_Days']      		= 'Днів';
$_['text_Hours']      		= 'Годин';
$_['text_Mins']      		= 'Хвилин';
$_['text_Secs']      		= 'Секунд';