<?php
// Heading
$_['heading_title'] = 'So Page builder';

// Text
$_['text_tax']      		= 'Додатково:';
$_['text_noproduct']      	= 'Немає елементів для показу!';

$_['shortcode_email']   			= 'Email';
$_['shortcode_email_desc']			= 'Введіть ваш admin email щоб отримати email.';
$_['shortcode_email_validation']   	= 'Email обовʼязковий';
$_['shortcode_email_validation_']	= 'Будь ласка, введіть валідну e-mail адресу';
$_['shortcode_name']   				= 'імʼя';
$_['shortcode_name_desc']			= 'Введіть тут інформацію, що буде показана в заголовку.';
$_['shortcode_name_validation']   	= 'Імʼя обовʼязково';
$_['shortcode_message']   			= 'Повідомлення';
$_['shortcode_message_validation']  = 'Повідомлення обовʼязково';
$_['shortcode_subject']   			= 'Тема';
$_['shortcode_subject_desc']		= 'Якщо ви оберете то тема буде показана у формі контактів.';
$_['shortcode_subject_validation']  = 'Тема обовʼязково';
$_['shortcode_send_success']  		= 'Ваше повідомлення надіслано успішно';
$_['shortcode_send_error']  		= 'Помилка на стороні сервера';
$_['shortcode_carousel_not_item_desc']= 'Контент каруселі не знайдено, перевірте налаштування каруселі.';