<?php

$_['text_history']			= 'Історія';
$_['text_shopping_cart']	= 'Кошик покупок';
$_['text_register']			= 'Реєстрація';
$_['text_account']			= 'Акаунт';
$_['text_download']			= 'Завантаження';
$_['text_login']			= 'Вхід';
$_['text_recent_products']	= 'недавно переглянуті продукти';
$_['text_my_account']		= 'Мій акаунт';
$_['text_new']				= 'Новинка';
$_['text_items_product']	= 'Є <span class="text-color">%s продукт(ів)</span> у вашому кошику';
$_['text_items']     	    = '<span class="items_cart">%s </span><span class="items_cart1">продукт(ів)</span><span class="items_cart2"> - %s</span>';
$_['button_cart']			= 'До кошика';
$_['text_search']			= 'Пошук';
$_['text_all_categories']	= 'Всі категорії';
$_['text_head_categories']	= 'Категорії';
$_['text_head_cart']		= 'Кошик';
$_['text_head_account']		= 'Акаунт';
$_['text_head_search']		= 'Пошук';
$_['text_head_recent_view']	= 'Переглянуті';
$_['text_head_gotop']		= 'Повернутися на верх';