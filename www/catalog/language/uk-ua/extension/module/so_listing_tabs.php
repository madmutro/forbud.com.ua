<?php
// Heading
$_['heading_title'] = 'So таби';

// Text
$_['text_tax']      	= 'Додатково:';
$_['value_price'] 		= 'Ціна';
$_['value_name'] 		= 'Назва';
$_['value_model'] 		= 'Модель';
$_['value_quantity'] 	= 'Кількість';
$_['value_rating'] 		= 'Рейтинг';
$_['value_sort_add'] 	= 'Sort Add';
$_['value_date_add'] 	= 'Дата додавання';
$_['value_sell'] 		= 'Топ продажів';
$_['text_noproduct']    = 'Немає продуктів для показу!';
$_['text_sale']      	= 'Знижка';
$_['text_new']      	= 'Новинка';