<?php
// Heading  
$_['heading_title']          = 'SOthemes обране';

// Text
$_['text_account']  = 'Акаунт';
$_['text_instock']  = 'В наявності';
$_['text_wishlist'] = 'Обране (%s)';
$_['text_title']             = 'Продукт доданий в обране';
$_['text_failure']           = 'Помилка';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_exists']            = 'Деякі елементи вже додані в обране';
$_['text_success']           = 'Успішно: Ви додали <a href="%s">%s</a> в ваше <a href="%s">Обране</a>!';
$_['text_login']             = 'Ви повинні <a href="%s">увійти</a> або <a href="%s">створити акаунт</a> щоб зберегти <a href="%s">%s</a> в ваше <a href="%s">Обране</a>!';
$_['text_remove']   	= 'Успішно: Ви змінили Ваше обране!';
$_['text_empty']    	= 'Список обраного пустий.';
?>