<?php
$_['lang_todaysdeal']          = 'Сьогоднішні знижки';
$_['view_details']          = 'Час акційної пропозиції закінчився';
$_['countdown_title_day']          = 'Дні';
$_['countdown_title_hour']          = 'Години';
$_['countdown_title_minute']          = 'хвилини';
$_['countdown_title_second']          = 'Секунди';
$_['overview']          = 'Перегляд';

// Quickview
$_['button_detail'] 		  = 'Деталі';
$_['text_category_stock_quantity']  = 'Швидше! тільки %s товар(ів) залишилось!';

// Home page
$_['text_more'] 			  = 'Більше';
$_['text_shop']  			  = 'Купити зараз';
$_['text_shop_cart']		  = 'Мій кошик';
$_['text_contact']      	  = 'Контакти';
$_['text_menu']  		 	  = 'Меню';
$_['text_compare']      	  = 'Порівняння (%s)';
$_['text_item_1']             = 'Вссього ';
$_['text_item_2']             = ' у вашому кошику';
$_['text_item_3']             = ' товар(ів)';

$_['text_backtotop']          = ' Повернутись на верх ';
$_['text_sub']                = ' Всі новини нашого магазину	';
$_['text_stock']              = ' Наявність ';
$_['text_price_old']          = ' Стара ціна ';
$_['text_price_save']         = ' Збереження ';
$_['text_price_sale']         = ' Акційна ціна ';
$_['text_viewdeals']          = ' Переглянути спец. пропозиції ';
$_['text_instock']            = ' В наявності ';
$_['text_show_cate']          = ' Показати всі категорії ';
$_['button_quickview']       = 'Додати в швидкий перегляд';

// Category page
$_['text_viewdeals']  		  = 'Подивитись всі спецпропозиції';