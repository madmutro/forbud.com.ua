<?php
// Text
$_['text_home']          = 'Головна';
$_['text_shopping_cart'] = 'Кошик покупок';
$_['text_account']       = 'Акаунт';
$_['text_register']      = 'Реєстрація';
$_['text_login']         = 'Логін';
$_['text_logout']        = 'Вийти';
$_['text_checkout']      = 'оформлення замовлення';
$_['text_search']        = 'Пошук';
$_['text_cart']        = 'Кошик';
$_['text_all']           = 'Показати все';
$_['text_more']       = 'Більше';
$_['text_language']       = 'Мова';
$_['text_currency']       = 'Валюта';
$_['text_compare']       = 'Порівняння';
$_['text_itemcount']     = '<span class="items_cart">%s </span>';

$_['text_needhelp']      = 'Потрібна допомога';
$_['text_emailus']       = 'Напишіть нам';
$_['text_morecategory']       = 'Більше категорій';