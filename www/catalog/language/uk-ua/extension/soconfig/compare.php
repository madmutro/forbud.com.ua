<?php
// Heading  
$_['heading_title']          = 'SOthemes Порівняння';

// Text
$_['text_title']             = 'Продукт додано до порівняння';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = 'Успішно: Ви додали <a href="%s">%s</a> в <a href="%s">порівняння</a>!';
$_['text_items']             = '%s';
$_['text_compare']   	   	 = 'Порівняння (%s)';
$_['text_failure']           = 'Помилка';
$_['text_exists']            = 'Деякі продукти вже в списку до порівняння';
// Error
$_['error_required']         = '%s обовʼязково!';

?>