<?php
// Heading  
$_['heading_title']          = 'SOthemes кошик';

// Text
$_['text_title']             = 'Продукт додано до кошика';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = '<a href="%s">%s</a> додано до <a href="%s">кошика</a>!';
$_['text_items']     = '%s продукт(ів) - %s';

// Error
$_['error_required']         = '%s обовʼязково!';

?>