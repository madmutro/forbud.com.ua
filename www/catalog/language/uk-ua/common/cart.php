<?php
// Text
$_['text_items']    = '%s товар(ів) - %s';
$_['text_empty']    = 'В кошику немає товарів :(';
$_['text_cart']     = 'Кошик';
$_['text_checkout'] = 'Оформлення купівлі';
$_['text_recurring']  = 'Платіжний профіль';