<?php
$_['entry_comment']        = 'Ваш відгук:';
$_['text_refine']          = 'Уточнити пошук';
$_['text_record']          = 'Записи';
$_['text_error']           = 'Сторінку не знайдено';
$_['text_empty']           = '';
$_['text_comments']        = 'Відгуків: ';
$_['text_compare']         = 'Порівняти (%s)';
$_['text_display']         = 'Вигляд:';
$_['text_list']            = 'Список';
$_['text_blog_page']       = 'сторінка';
$_['text_grid']            = 'Вітрина';
$_['text_sort']            = 'Сортувати:';
$_['text_default']         = 'Стандартно';

$_['text_name_asc']        = 'За ім&#39ям від А до Я';
$_['text_name_desc']       = 'За ім&#39ям от Я до А';
$_['text_sort_asc']        = 'За порядком: за зростанням';
$_['text_sort_desc']       = 'За порядком: за зменшенням';
$_['text_date_added_asc']  = 'За датою: за зростанням';
$_['text_date_added_desc'] = 'За датою: за зменшенням';
$_['text_rating_desc']     = 'За рейтингом: за зростанням';
$_['text_rating_asc']      = 'За рейтингом: за зменшенням';
$_['text_popular_desc']    = 'За популярністю: за зростанням';
$_['text_popular_asc']     = 'За популярністю: за зменшенням';
$_['text_comments_desc']   = 'За відгуками: за зростанням';
$_['text_comments_asc']    = 'За відгуками: за зменшенням';

$_['text_model_asc']       = 'Моделі від А до Я';
$_['text_model_desc']      = 'Модели від Я до А';

$_['text_limit']           = 'На сторінку:';
$_['text_sort']            = 'Сортування за:';

$_['text_viewed']          = 'Переглядів:';
$_['text_further']         = '&rarr;';
$_['text_january']         = "січня";
$_['text_february']        = "лютого";
$_['text_march']           = "березня";
$_['text_april']           = "квітня";
$_['text_may']             = "травня";
$_['text_june']            = "червня";
$_['text_july']            = "липня";
$_['text_august']          = "серпня";
$_['text_september']       = "вересня";
$_['text_october']         = "жовтня";
$_['text_november']        = "листопад";
$_['text_december']        = "грудня";
$_['text_today']           = "Сьогодні";
$_['text_date']            = "d M Y";
$_['text_hours']           = " H:i:s";
$_['text_edit']            = "Редагувати";
$_['text_blog']            = "Категорія: ";
$_['text_category']        = "Категорія товару: ";
$_['text_category_record']   = 'Категорія: ';
$_['text_record']          = "Запис: ";
$_['text_product']         = "Товар: ";
$_['text_manufacturer']    = 'Віробник: ';
$_['text_karma'] 			= "Корисність: ";
$_['text_buy']     			= '<span class="seocmspro_buy">Купував на сайті</span>';
$_['text_buyproduct']     	= '<span class="seocmspro_buy">Купував цей товар</span>';
$_['text_registered']     	= '<span class="seocmspro_buy">Зареєстрований</span>';
$_['text_admin']    	 	= '<span class="seocmspro_buy">Адміністратор</span>';
$_['text_buy_ghost']   	 	= '<span class="seocmspro_buy">Гість</span>';
$_['text_author']            = 'Автор: ';

$_['error_addfields_name'] = 'Невірно вказано назву додаткового поля';
$_['button_continue']      = "Далі";
$_['entry_ans']            = 'Ваша відповідь:';
$_['entry_rating']         = 'Оцініть публікацію: ';
$_['entry_rating_review']  = 'Дайте оцінку: ';
$_['entry_minus']           = 'Жахливо';
$_['entry_bad']             = 'Погано';
$_['entry_normal']          = 'Задовільно';
$_['entry_good']            = 'Добре';
$_['entry_exelent']			= 'Відмінно';
$_['entry_records_more']	= 'Показати ще ';
$_['entry_records_more_end']= '...';

$_['separator_center'] 		= '|';
$_['separator_next'] 		= '&rarr;';
$_['separator_previus'] 	= '&larr;';

if (!isset($_['text_separator'])) {
	$_['text_separator']        = ' &raquo; ';
}

