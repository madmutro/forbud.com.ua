<?php
$_['lang_todaysdeal']          = 'Сегодняшние скидки';
$_['view_details']          = 'Время акционного предложения закончилось';
$_['countdown_title_day']          = 'Дни';
$_['countdown_title_hour']          = 'Часы';
$_['countdown_title_minute']          = 'минуты';
$_['countdown_title_second']          = 'Секунды';
$_['overview']          = 'Просмотр';

// Quickview
$_['button_detail'] 		  = 'Детали';
$_['text_category_stock_quantity']  = 'Поторопись! только %s товар(ов) осталось!';

// Home page
$_['text_more'] 			  = 'Больше';
$_['text_shop']  			  = 'Купить сейчас';
$_['text_shop_cart']		  = 'Моя корзина';
$_['text_contact']      	  = 'Контакты';
$_['text_menu']  		 	  = 'Меню';
$_['text_compare']      	  = 'Сравнение (%s)';
$_['text_item_1']             = 'Всего ';
$_['text_item_2']             = ' в вашей корзине';
$_['text_item_3']             = ' товар(ов)';

$_['text_backtotop']          = ' Вернуться на верх ';
$_['text_sub']                = ' Все новости нашего магазина	';
$_['text_stock']              = ' Наличие ';
$_['text_price_old']          = ' Старая цена ';
$_['text_price_save']         = ' Price Save ';
$_['text_price_sale']         = ' Скидочная цена ';
$_['text_viewdeals']          = ' Посмотреть спец. предложения ';
$_['text_instock']            = ' В наличии ';
$_['text_show_cate']          = ' Показать все категории ';
$_['button_quickview']       = 'Добавить в быстрый просмотр';

// Category page
$_['text_viewdeals']  		  = 'Посмотреть все спецпредложения';