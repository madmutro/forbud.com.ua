<?php
// Heading  
$_['heading_title']          = 'SOthemes Избраное';

// Text
$_['text_account']  = 'Акаунт';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Избраное (%s)';
$_['text_title']             = 'Продукт добавлен в избраное';
$_['text_failure']           = 'Ошибка';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_exists']            = 'Некоторые елементы уже добавлены в избраное';
$_['text_success']           = 'Успешно: Вы дабавили <a href="%s">%s</a> в ваше <a href="%s">Избраное</a>!';
$_['text_login']             = 'Вы должны <a href="%s">войти</a> или <a href="%s">создать акаунт</a> чтобы сберечь <a href="%s">%s</a> в ваше <a href="%s">Избраное</a>!';
$_['text_remove']   	= 'Успешно: Вы изменили Ваше избраное!';
$_['text_empty']    	= 'Список избраного пустой.';
?>