<?php
// Heading  
$_['heading_title']          = 'SOthemes Сравнение';

// Text
$_['text_title']             = 'Продукт добавлен к сравнение';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = 'Успешно: Вы добавили <a href="%s">%s</a> в <a href="%s">сравнение</a>!';
$_['text_items']             = '%s';
$_['text_compare']   	   	 = 'Сравнение (%s)';
$_['text_failure']           = 'Ошибка';
$_['text_exists']            = 'Некоторые продукты уже в списке к сравнению';
// Error
$_['error_required']         = '%s обязательно!';

?>