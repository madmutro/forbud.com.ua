<?php
// Heading  
$_['heading_title']          = 'SOthemes корзина';

// Text
$_['text_title']             = 'Продукт добавлен в корзину';
$_['text_thumb']             = '<img src="%s" alt="" />';
$_['text_success']           = '<a href="%s">%s</a> добавлен в <a href="%s">корзину</a>!';
$_['text_items']     = '%s продукт(ов) - %s';

// Error
$_['error_required']         = '%s обязательно!';

?>