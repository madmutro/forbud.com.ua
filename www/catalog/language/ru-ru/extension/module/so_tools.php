<?php

$_['text_history']			= 'История';
$_['text_shopping_cart']	= 'Корзина покупок';
$_['text_register']			= 'Регистрация';
$_['text_account']			= 'Акаунт';
$_['text_download']			= 'Загрузки';
$_['text_login']			= 'Вход';
$_['text_recent_products']	= 'недавно просмотреные продукты';
$_['text_my_account']		= 'Мой акаунт';
$_['text_new']				= 'Новинка';
$_['text_items_product']	= 'Есть <span class="text-color">%s продукт(ов)</span> в вашей корзине';
$_['text_items']     	    = '<span class="items_cart">%s </span><span class="items_cart1">продукт(ов)</span><span class="items_cart2"> - %s</span>';
$_['button_cart']			= 'Добавить в корзину';
$_['text_search']			= 'Поиск';
$_['text_all_categories']	= 'Все категории';
$_['text_head_categories']	= 'Категории';
$_['text_head_cart']		= 'Корзина';
$_['text_head_account']		= 'Акаунт';
$_['text_head_search']		= 'Поиск';
$_['text_head_recent_view']	= 'Просмотреные';
$_['text_head_gotop']		= 'Вернуться на верх';