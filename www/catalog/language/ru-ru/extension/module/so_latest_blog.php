<?php
// Heading
$_['text_latest']  = 'Последние';
$_['text_comment']     = ' Коментарий';
$_['text_comments']    = ' Коментариев';
$_['text_view']        = ' Посмотреть все';
$_['text_views']       = ' Просмотры';
// Text
$_['text_tax']      = 'Дополниетльно:';
$_['text_noitem']      = 'Нет елементов для показа!';
$_['text_no_database'] = 'Не установлена База Данных для модуля, установите модуль "Simple Blog" сейчас!';