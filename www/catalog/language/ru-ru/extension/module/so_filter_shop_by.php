<?php
// Heading
$_['heading_title'] = 'So Фильтр';

// Text
$_['text_tax']      		= 'Дополнительно:';
$_['text_noproduct']      	= 'нет продуктов для показа!';
$_['text_sale']      		= 'Скидка';
$_['text_new']      		= 'Новинка';

$_['text_search']      		= 'Поиск';
$_['text_price']      		= 'Цена';
$_['text_reset_all']      	= 'Очистить';
$_['text_manufacturer']     = 'Производитель';
$_['text_subcategory']     	= 'Подкатегория';