<?php
// Heading
$_['heading_title'] = 'So табы';

// Text
$_['text_tax']      	= 'Дополнительно:';
$_['value_price'] 		= 'Цена';
$_['value_name'] 		= 'Название';
$_['value_model'] 		= 'Модель';
$_['value_quantity'] 	= 'Количество';
$_['value_rating'] 		= 'Рейтинг';
$_['value_sort_add'] 	= 'Sort Add';
$_['value_date_add'] 	= 'Дата добавления';
$_['value_sell'] 		= 'Топ продаж';
$_['text_noproduct']    = 'Нет продуктов для показа!';
$_['text_sale']      	= 'Скидка';
$_['text_new']      	= 'Новинка';