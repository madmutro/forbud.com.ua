<?php
// Heading
$_['heading_title'] = 'So скидки';

// Text
$_['text_tax']      		= 'Дополниетльно:';
$_['text_noitem']      		= 'Нет елементов для показа!';
$_['text_sale']      		= 'Скидка';
$_['text_new']      		= 'Новинка';

$_['text_Day']      		= 'День';
$_['text_Hour']      		= 'Час';
$_['text_Min']      		= 'Минута';
$_['text_Sec']      		= 'Секунда';
$_['text_Days']      		= 'Дней';
$_['text_Hours']      		= 'Часов';
$_['text_Mins']      		= 'Минут';
$_['text_Secs']      		= 'Секунд';