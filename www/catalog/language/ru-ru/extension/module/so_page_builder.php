<?php
// Heading
$_['heading_title'] = 'So Page builder';

// Text
$_['text_tax']      		= 'Дополниетльно:';
$_['text_noproduct']      	= 'Нет елементов для показа!';

$_['shortcode_email']   			= 'Email';
$_['shortcode_email_desc']			= 'Введите ваш admin email чтобы получить email.';
$_['shortcode_email_validation']   	= 'Email обязательный';
$_['shortcode_email_validation_']	= 'Пожайлуйста введите валидный e-mail адрес';
$_['shortcode_name']   				= 'имя';
$_['shortcode_name_desc']			= 'Введите имя здесь информацию что будет показана в заголовкеe.';
$_['shortcode_name_validation']   	= 'Имя обязательно';
$_['shortcode_message']   			= 'Сообщение';
$_['shortcode_message_validation']  = 'Сообщение обязательно';
$_['shortcode_subject']   			= 'Тема';
$_['shortcode_subject_desc']		= 'Если вы выберете да то тема будет показана в форме контактов.';
$_['shortcode_subject_validation']  = 'Тема обязательна';
$_['shortcode_send_success']  		= 'Ваше сообщение отослано успешно';
$_['shortcode_send_error']  		= 'Ошибка на стороне сервера';
$_['shortcode_carousel_not_item_desc']= 'Контент карусели не найден, проверьте настройки карусели.';