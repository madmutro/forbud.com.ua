<?php
// Heading
$_['heading_title'] 								= 'So Onepage Checkout';
$_['text_checkout_create_account_login']           	= 'Создать акаунт или войти';
$_['text_confirm_order']           					= 'Подтвердить заказ';
$_['text_shipping_address']           				= 'Адрес доставки';
$_['text_title_shipping_method']           			= 'Метод доставки';
$_['text_title_payment_method']           			= 'Метод оплаты';
$_['text_coupon_voucher']           				= 'Вы имеете купон или ваучер?';
$_['text_enter_coupon_code']           				= 'Введите код купона';
$_['text_enter_voucher_code']           			= 'Введите код ваучера';
$_['text_apply_voucher']           					= 'Использовать ваучер';
$_['text_enter_reward_points']           			= 'Введите бонусные балы';
$_['text_apply_points']           					= 'Использовать балы';
$_['text_shopping_cart']           					= 'Страница заказа';
$_['text_payment_detail']          					= 'Детали оплаты';

//Error
$_['error_comment']									= 'Вы должны добавить коментарий к заказу.';