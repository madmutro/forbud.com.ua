<?php
// Text
$_['text_search']    	= 'Введите здесь ключевые слова...';
$_['text_category_all'] = 'Все категории';
$_['text_tax']      	= 'Налоги';
$_['text_price']      	= 'Цена';
$_['button_cart']       = 'Добавить в корзину';
$_['button_wishlist']       = 'Добавить в избраное';
$_['button_compare']       = 'Добавить в сравнение';
