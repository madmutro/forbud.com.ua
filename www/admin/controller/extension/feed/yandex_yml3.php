<?php
/**
* Yandex.YML data feed for OpenCart (ocStore) 2.3.x
*
* Controller of admin form
*
* @author Alexander Toporkov <toporchillo@gmail.com>
* @copyright (C) 2012- Alexander Toporkov
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*
* Latest version of this module: http://opencartforum.ru/files/file/670-eksport-v-iandeksmarket/
*/
require_once(dirname(__FILE__).'/yandex_yml.php');

class ControllerExtensionFeedYandexYml3 extends ControllerExtensionFeedYandexYml {

	protected $CONFIG_PREFIX = 'yandex_yml3_';

	public function index() {
		$this->load->language('extension/feed/yandex_yml');
		$this->load->language('extension/feed/yandex_yml3');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate($this->request->post))) {
			$this->preparePostData();

			$to_save = array();
			foreach($this->request->post as $key=>$val) {
				$key = str_replace('yandex_yml_', 'yandex_yml3_', $key);
				$to_save[$key] = $val;
			}
			$this->model_setting_setting->editSetting('yandex_yml3', $to_save);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL'));
		}

		$this->setLanguageData();
		$this->tdata['token'] = $this->session->data['token'];
		$this->tdata['cron_path'] = 'php '.realpath(DIR_CATALOG.'../export/yandex_yml3.php');

		$this->tdata['export_url'] = HTTP_CATALOG.'export/';

		if (isset($this->error['warning'])) {
			$this->tdata['error_warning'] = $this->error['warning'];
		} else {
			$this->tdata['error_warning'] = '';
		}

		$this->tdata['action'] = $this->url->link('extension/feed/yandex_yml3', 'token=' . $this->session->data['token'], 'SSL');

		$this->tdata['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=feed', 'SSL');

		$this->tdata['data_feed'] = HTTP_CATALOG . 'index.php?route=extension/feed/yandex_yml3';

		$this->setFormData();
		
		$template = 'extension/feed/yandex_yml.tpl';
		
		$this->tdata['header'] = $this->load->controller('common/header');
		$this->tdata['column_left'] = $this->load->controller('common/column_left');
		$this->tdata['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($template, $this->tdata));
	}

	public function install() {
	}

	public function uninstall() {
	}
}
