<?php
class ControllerExtensionModuleCustomfields extends Controller {
	private $error = array();

	public function index() {
		// add languages
		$this->load->language('extension/module/customfields');
		// add models
		$this->load->model('extension/module/customfields');
		// if save or apply button		
		$data['ret'] = '';
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			// save fields with $this->request->post
			$fields = array();
			$i = 1;
			while ($i) {
				if (isset($this->request->post['name-'.$i])) {
					$fields[] = array(
						'name' => $this->request->post['name-'.$i],
						'type' => $this->request->post['type-'.$i],
						'description' => $this->request->post['descr-'.$i],
						'value' => $this->request->post['value-'.$i]
					);
					$i++;
				}	else {
					$i = 0;
				}			
			}
			
			$data['ret'] = $this->model_extension_module_customfields->writeAllFields($fields);
			
			$this->session->data['success'] = $this->language->get('text_success');
		//if apply - reload to clear $_post
      if (isset($this->request->get['apply'])) {
		    $this->response->redirect($this->url->link('extension/module/customfields', 'token=' . $this->session->data['token'], true));
      } else {
		    $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], true));
      }
		}
		// if $post empty
		// set title
		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title'] = $this->language->get('heading_title');

		// check for errors
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		// check for messages
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

      unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		// make breadcrumbs
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_module'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/customfields', 'token=' . $this->session->data['token'], true)
		);
	// make links for buttons
  	$data['save'] = $this->url->link('extension/module/customfields', 'token=' . $this->session->data['token'], true);
    $data['apply'] = $this->url->link('extension/module/customfields', 'token=' . $this->session->data['token'] . '&apply', true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true);

    $data['token'] = $this->session->data['token'];
	
	// types of fields
    $data['types'] = array(
      'text' => 'Текст',
      'image' => 'Картинка'
    );
	
	// load current fields
	$data['fields'] = $this->model_extension_module_customfields->getAllFields();
	// load template parts
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

    $this->response->setOutput($this->load->view('extension/module/customfields', $data));
	}
	
	// standart validate	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/customfields')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}