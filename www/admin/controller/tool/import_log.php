<?php

/**
 * @author Artem Pitov
 * @link pitov.pro
 */

class ControllerToolImportLog extends Controller {
	
	private $error 		 = array();
	private $error_log   = DIR_APPLICATION . 'uploads/errors.tmp';
	private $report_log  = DIR_APPLICATION . 'uploads/report.tmp';
	private $sos_log  	 = DIR_APPLICATION . 'uploads/sos.tmp';

	public function index() 
	{

		$data = $this->load->language('tool/import_log');
		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['breadcrumbs'] = 
		[
			[
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
			],
			[
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('tool/import_log', 'token=' . $this->session->data['token'], true)
			]
		];

		$data['clear']            = $this->url->link('tool/import_log/clear', 'token=' . $this->session->data['token'], true);	
		$data['error_log']        = '';
		$data['report_log']       = '';
		$data['sos_log']		  = '';
		$data['error_filemtime']  = '';
		$data['report_filemtime'] = '';
		$data['sos_filemtime']    = '';

		if ( file_exists($this->error_log) ) {
			$data['error_log']       = file_get_contents($this->error_log, FILE_USE_INCLUDE_PATH, null);
			$data['error_filemtime'] = sprintf($this->language->get('text_date_update'), date('d.m.Y H:i:s e', filemtime($this->error_log)));
		}

		if ( file_exists($this->report_log) ) {
			$data['report_log']       = file_get_contents($this->report_log, FILE_USE_INCLUDE_PATH, null);	
			$data['report_filemtime'] = sprintf($this->language->get('text_date_update'), date('d.m.Y H:i:s e', filemtime($this->report_log)));
		}

		if ( file_exists($this->sos_log) ) {
			$data['sos_log']       = file_get_contents($this->sos_log, FILE_USE_INCLUDE_PATH, null);	
			$data['sos_filemtime'] = sprintf($this->language->get('text_date_update'), date('d.m.Y H:i:s e', filemtime($this->sos_log)));
		}		

		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->Controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


		$v = explode('.', VERSION);

		if ($v[1] > 2)
			$this->response->setOutput($this->load->view('tool/import_log', $data));
		else 
			$this->response->setOutput($this->load->view('tool/import_log.tpl', $data));
	}
	
	public function clear() 
	{
		$this->load->language('tool/log');

		if (!$this->user->hasPermission('modify', 'tool/import_log')) {
			$this->session->data['error'] = $this->language->get('error_permission');
		} else {
			
			if ( file_exists($this->error_log) )  
				unlink($this->error_log);
			
			if ( file_exists($this->report_log) ) 
				unlink($this->report_log);

			if ( file_exists($this->sos_log) )    
				unlink($this->sos_log);

			/*$handle = fopen($this->error_log, 'w+');
			fclose($handle);

			$handle = fopen($this->report_log, 'w+');
			fclose($handle);*/			

			$this->session->data['success'] = $this->language->get('text_success');
		}

		$this->response->redirect($this->url->link('tool/import_log', 'token=' . $this->session->data['token'], true));
	}
}
