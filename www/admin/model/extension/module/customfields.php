<?php
class ModelExtensionModuleCustomfields extends Model {
	// function to get all fields
	public function getAllFields() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `oc_customfields_option` (
  `row_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) NOT NULL DEFAULT 'text',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `value` varchar(5535) NOT NULL DEFAULT '',
  PRIMARY KEY (`row_id`),
  KEY `name` (`name`)
) CHARSET=utf8;");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customfields_option");
		return $query->rows;
	}
	// function to write all fields
	public function writeAllFields($data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "customfields_option");
		foreach ($data as $attr) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customfields_option SET type = '" . $this->db->escape($attr['type']) . "', name = '" . $this->db->escape($attr['name']) . "', description = '" . $this->db->escape($attr['description']) . "', value = '" . $this->db->escape($attr['value']) . "'");
		}
		return 'ok';
	}
}
?>