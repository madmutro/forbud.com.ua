<?php
// Heading
$_['heading_title']	   = 'Журнал работы модуля "Автоматическая обработка прайс-листов"';

// Text
$_['text_success']	   = 'Журнал ошибок успешно очищен!';
$_['text_list']        = 'Журнал импорта-экспорта';
$_['text_date_update'] = 'Последнее обновление файла было <b>%s</b>';
$_['text_log_error']   = '<b>Ошибки (errors.tmp)</b>';
$_['text_log_report']  = '<b>Отчёт (report.tmp)</b>';
$_['text_log_sos']     = '<b>Последняя обработанная строка (sos.tmp)</b>';

// Error
$_['error_permission'] = 'У вас нет прав что бы очистить журнал ошибок!';
