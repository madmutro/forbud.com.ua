<?php
// Heading
$_['heading_title']    			= 'Категории товаров (Новые, Рекомендуемые, Случайные)';

// Text
$_['text_extension']      		= 'Расширения';
$_['text_success']     			= 'Модуль успешно изменен!';
$_['text_edit']        			= 'Изменить модуль';
$_['text_wall']        			= 'Стена';
$_['text_carousel']    	  		= 'Карусель';
$_['text_new_categories'] 		= 'Новые категории';
$_['text_featured_categories'] 	= 'Рекомендуемые категории';
$_['text_random_categories']	= 'Случайные категории';

// Entry
$_['entry_name'] 	   			= 'Название модуля';
$_['entry_title'] 	   			= 'Заголовок';
$_['entry_show_categories']     = 'Показывать категории';
$_['entry_type']       			= 'Тип отображения';
$_['entry_featured_category']   = 'Рекомендуемые категории';
$_['entry_limit']     		    = 'Лимит';
$_['entry_width']      		    = 'Ширина';
$_['entry_height']              = 'Высота';
$_['entry_status']              = 'Статус';

// Help
$_['help_featured_category']    = 'Добавьте категории если выбрали Рекомендуемые категории';

// Error
$_['error_permission'] 			= 'У вас недостаточно прав для редактирования модуля';
$_['error_name'] 	   			= 'Название модуля должно содержать от 3 до 64 символов!';
$_['error_width'] 	   			= 'Введите ширину изображения!';
$_['error_height']     			= 'Введите высоту изображения!';