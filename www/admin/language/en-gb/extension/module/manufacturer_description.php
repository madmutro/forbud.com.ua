<?php
##################################################################
#    Manufacturer Description 1.20 for Opencart 23x by AlexDW    #
##################################################################
$_['heading_title']    = '<i class="fa fa-cubes fa-lg" style="color: #5BC0DE"></i> Manufacturer Description - Description and Meta tags for Manufacturers [230x]';

// Text
$_['text_module']		= 'Modules';
$_['text_edit']			= 'Edit Manufacturer Description Module';
$_['text_success']		= 'Success: You have modified module Manufacturer Description!';

$_['text_settings']		= 'Settings';
$_['text_server']		= 'Server: ';
$_['text_host']			= 'Host: ';

$_['text_before']		= 'before products';
$_['text_after']		= 'after products';
$_['image_size']		= 'Logo size, px';

$_['help_temp']		= '<i class="fa fa-check"></i> Output a description by means of the template.</br> Some templates already provide the output of the description and logo of the manufacturer, if any. If you use such template, enable this setting to avoid double output.';
$_['help_pos']		= '<i class="fa fa-check"></i> Displays the description in the top (before the goods) or from below (after the goods)</br> The bottom output may not work if your template uses AJAX pagination, i.e. dynamic loading of goods as you reach the bottom of the page.';

// Entry
$_['entry_status']		= 'Module status';
$_['entry_image']		= 'Show logo';
$_['entry_temp']		= 'Use template output';
$_['entry_sizeh']		= 'height';
$_['entry_sizew']		= 'width';
$_['entry_pos']			= 'Show description';
$_['entry_code']      	= 'License';

// Error
$_['error_permission']	= 'Warning: You do not have permission to modify module Manufacturer Description!';
?>