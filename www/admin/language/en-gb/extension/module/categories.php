<?php
// Heading
$_['heading_title']    			= 'Product categories';

// Text
$_['text_extension']      		= 'Extension';
$_['text_success']     			= 'Success: You have modified Product Categories module!';
$_['text_edit']        			= 'Edit Product Categories Module';
$_['text_wall']        			= 'Wall';
$_['text_carousel']    			= 'Carousel';
$_['text_new_categories'] 		= 'New Categories';
$_['text_featured_categories'] 	= 'Featured Categories';
$_['text_random_categories']	= 'Random ategories';

// Entry
$_['entry_name']       			= 'Module Name';
$_['entry_title'] 	   			= 'Title';
$_['entry_show_categories']     = 'Show categories';
$_['entry_featured_category']   = 'Featured Categories';
$_['entry_type']       			= 'Type';
$_['entry_limit']     			= 'Limit';
$_['entry_width']      			= 'Width';
$_['entry_height']     			= 'Height';
$_['entry_status']     			= 'Status';

// Help
$_['help_featured_category']    = 'Add the category if you selected the Category Featured';

// Error
$_['error_permission'] 			= 'Warning: You do not have permission to modify categories module!';
$_['error_name']       			= 'Module Name must be between 3 and 64 characters!';
$_['error_width']      			= 'Width required!';
$_['error_height']     			= 'Height required!';