<?php
// Heading
$_['heading_title']          = 'Complete Rich Snippet By All In One Seo Module';
$_['text_success']           = 'Success: You have modified Rich Snippets!'; 
$_['grsnippet']              = 'Google Rich Snippets For Seo';
$_['business_type']          = 'Buisness Type';
$_['payment_method']         = 'Select Payment Method';
$_['delivery_method']        = 'Select Delivery Method';
$_['text_form']   			 = 'Fill in form details';
$_['entry_google_status']    = 'Google Rich Snippet Status';
$_['entry_facebook_status']    = 'Facebook Rich Snippet Status';
$_['entry_twitter_status']    = 'Twitter Rich Snippet Status';
$_['entry_pinterest_status']    = 'Pinterest Rich Snippet Status';
$_['button_save']            = 'Save';
$_['tab_achieve']            = 'What you will achieve?';
$_['tab_verify']          	 = 'Verify your snippets';
$_['tab_company']            = 'Form Details Page';
$_['text_googlepageid_help'] = "If this is provided to us, we will link your store with Google plus page.";
$_['text_facebookadminid_help'] = "If this is provided to us, we will link your store with give your Facebook page.";
$_['text_twitterusername_help'] = "If this is provided to us, we will link your store with give your Twiiter account. Required for twitter rich cards activation. Ex Username: opencart_store";
$_['text_googlepageid']      = 'Google Plus Page Id';
$_['text_twitterusername']     = 'Twitter User Name';
$_['text_twittercardsize']     = 'Twitter Card Size';
$_['text_facebookadminid']     = 'Facebook Admin Id';
$_['text_multistoresupport']     = 'We support multistore for rich snippets. Please add more details in System - Store - Settings - All In One Seo Tab. <a href="%s">Click here</a> to see setting.';
$_['company_name'] 		    = 'Store Name';
$_['country_name']  		= 'Country Name';
$_['locality_name'] 		= 'Locality Name';
$_['postal_code'] 		    = 'Postal Code';
$_['street_address'] 		= 'Street Address';
$_['telephone_number'] 		= 'Telephone Number';
$_['check_impact']         = 'Go to next tab above to know how this field is most important';
$_['store_help'] 		=  'Fill the form below to make best use of all the snippets.<br>Details below will be used by snippets designed for Facebook, Google, Twitter.';
$_['verify_help'] 		= 'Steps to verify rich snippets your store';
$_['achieve_help'] 		= 'What you will achieve is important, have a look below:';
$_['twitter_help'] 		= 'Following details below will be inserted on product page in rdfa format.<br>This rdfa format details are understood by Search Engine Google.';
$_['text_about']   = "<h3>What is rich snippet?</h3>Rich Snippets are extra code that is added on page. This code can be read by various search engines and social networking sites.<br><br><h3>What are benefits?</h3>It adds a nice display of your website in search results and social sites. For ex. Seeing rating and price in google search results. Seeing price and stock availability of products for your pins on your pin board directly.<br><br><h3>How to use it?</h3>It is simple. Fill in the details below and save it. Once you save then go to let us verify tab and verify if it working or not.";
?>