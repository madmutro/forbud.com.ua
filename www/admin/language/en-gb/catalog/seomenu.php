<?php
// Text

$_['text_headingtitle']       			   = 'ALL IN ONE SEO';

$_['text_generators']                  = 'Generator/Editor/Settings';
$_['text_autogenerators']              = 'Auto Generators';
$_['text_autogenerators_mm']           = 'Generators';
$_['text_editor']                      = 'Seo Advanced Editor';
$_['text_editor_mm']                   = 'Seo Editor';
$_['text_report']                      = 'Seo Reports';

$_['text_category']                    = 'Categories';
$_['text_product']                     = 'Products';
$_['text_manufacturer']                = 'Manufacturers';
$_['text_information']                 = 'Information';
$_['text_general']                     = 'General Pages';

$_['text_socialrichsnippets']          = 'Social / Search Engine Helper';
$_['text_richsnippets']                = 'Rich Snippets';
$_['text_google']                      = 'Google';
$_['text_facebook']                    = 'Facebook';
$_['text_twitter']                     = 'Twitter';
$_['text_pinterest']                   = 'Pinterest';

$_['text_sitemap']                     = 'Sitemap';
$_['text_robots']                      = 'Robots.txt';

$_['text_seoredirectmanager']          = 'Seo Redirect Manager';
$_['text_redirectmanager']             = 'Redirect Manager';
$_['text_failedlinks']                 = 'Failed Links';

$_['text_clearseotool']                = 'Clear Seo Tool';
$_['text_clearseo']                    = 'Clear Seo';
$_['text_seo_setting']                     = 'Seo Settings';
$_['text_more']                        = 'More';

$_['text_aboutallinoneseo']            = 'About';
$_['text_description']                 = 'All in one seo extension for opencart is designed by <a href="http://store.nerdherd.in/" target="_blank" title="Nerdherd">Nerdherd</a>. The extension helps to provide you with all seo features in one module. Version: 34.1.1';