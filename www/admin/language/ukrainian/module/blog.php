<?php
include('version.php');
$_['url_module_text']           = 'SEO CMS';

$_['sc_version'] 				= $_['blog_version'] ;
$_['sc_model'] 					= $_['url_module_text'];
$_['text_widget_seocms']        = $_['url_module_text'];
$_['widget_seocms_version']     = $_['blog_version'];



$_['ocmod_seocms_name'] = $_['url_module_text'];
$_['ocmod_seocms_version'] = $_['blog_version'];
$_['ocmod_seocms_mod'] = 'seocms';
$_['ocmod_seocms_author'] = 'opencartadmin.com';
$_['ocmod_seocms_link'] = 'https://opencartadmin.com';
$_['ocmod_seocms_html'] = 'Модифікатор для SEO CMS успішно встановлений';

$_['url_create_text']           = '<div style="text-align: center; text-decoration: none;">Створення та оновлення<br>таблиць та даних для модуля<br><ins style="text-align: center; text-decoration: none; font-size: 13px;">(при установці та оновленні модуля)</ins></div>';
$_['url_delete_text']           = '<div style="text-align: center; text-decoration: none;">Видалити налаштування старих версій<br><ins style="text-align: center; text-decoration: none; font-size: 13px;">(при установці та оновленні модуля, після <br>перезбереження "налаштувань", "схем" та "віджетів")</ins></div>';
$_['url_back_text']             = 'В налаштування модуля';
$_['url_modules_text']          = 'В перелік модулів';
$_['url_blog_text']             = 'Категорії';
$_['url_record_text']           = 'Записи';
$_['url_fields_text']           = 'Поля';
$_['url_comment_text']          = 'Коментарі';
$_['url_blog_text']             = 'Категорії';
$_['url_record_text']           = 'Записи';
$_['url_comment_text']          = 'Коментарі';
$_['url_review_text'] 			= 'Відгуки';
$_['url_forum_buy_text']        = 'Технічна підтримка';
$_['url_forum_update_text']     = 'Отримати оновлення';
$_['url_forum_text']            = 'Технічна підтримка';
$_['url_forum_site_text']       = 'Форум';
$_['url_opencartadmin_text']    = 'Про модуль';
$_['url_forum_buy']          = 'https://opencartforum.com/files/file/2371-seo-cms-top-2-blog-novosti-otzyvy-galereya-formy/';
$_['url_forum']              = 'https://opencartforum.com/topic/45395-seo-cms-top-2-blog-novosti-otzyvy-galereya-formy/';
$_['url_opencartadmin']      = 'https://opencartadmin.com';
$_['entry_download_new_version'] = 'Завантажити останню версію';
$_['url_download_new_version'] = 'https://opencartadmin.com/index.php?route=account/download';

$_['heading_title']             = '<div class="seocmsht" style="overflow: hidden; height: 15px; line-height: 15px; vertical-align: middle; font-size: 15px; color: green;"><div style="float: left;"><img src="view/image/blog-icon.png"></div><div style="float: left;">&nbsp;&nbsp;SEO CMS <ins class="seo_cms_ins" style="font-size: 9px; text-decoration: none;">(by <a href="http://opencartadmin.com" style="color: green;">opencartadmin.com</a>)</ins></div></div><script>$(\'.seocmsht:eq(1)\').closest(\'tr\').hide();</script>';
$_['heading_dev']               = 'Розробник <a href="mailto:admin@opencartadmin.com" target="_blank">opencartadmin.com</a><br>&copy; 2011-'.date('Y') .' All Rights Reserved';
$_['text_new_version']          = "<div style='background: #FFCFCE; border: 2px solid red; padding: 10px;'>Встановлена версія модуля: <b><span style='color: red;'>".$_['blog_version']."</span></b><br>"."Остання версія модуля: <span style='color: green;'><b>";

$_['text_update_version_begin']          = "<div style='background: #F7FFF2; width: auto;  border: 1px solid #E2EDDC; padding: 10px;'>Остання доступна версія модуля: <span style='font-size: 21px;'>";
$_['text_update_version_end']            = "</span></div>";

$_['text_mod_refresh']            = "Модифікатори оновлено<br>";
$_['text_mod_author']          = 'admin@opencartadmin.com';

$_['text_mod_add_seocms'] = 'SEO CMS модифікатор встановлено<br>';

$_['text_separator']          = ' > ';

$_['text_new_version_end']      = '</b></span><br>Рекомендується: <span style="color: green;"><b>оновіть модуль до останньої версії</b></span></div>';

$_['entry_cache_mobile_detect'] = 'Визначення мобільного пристрою <br>для кешування';

$_['text_not_found']			= 'ЧПУ (not_found)';
$_['text_update_text']               = 'Натисніть кнопку: <br><span style="color: #FFA500; text-align: center;">"Створення та оновлення<br>таблиць та даних модуля<br>(при установці та оновленні модуля)"</span><br>Для оновлення модуля <br>Після чого перезбережіть алаштування <br>головної модуля, схемы та віджети <br>(пункти у верхньому меню), <br>щоб нові параметри почали діяти';
$_['text_module']               = 'Модулі';
$_['text_add']                  = 'Додати';
$_['text_action']               = 'Дія:';
$_['text_success']              = 'Модуль оновлено!';
$_['text_content_top']          = 'Вміст шапки';
$_['text_content_bottom']       = 'Вміст підвалу';
$_['text_column_left']          = 'Ліва колонка';
$_['text_column_right']         = 'Права колонка';
$_['text_what_lastest']         = 'Останні записи';
$_['text_select_all']           = 'Виділити все';
$_['text_unselect_all']         = 'Зняти виділення';
$_['text_blog']                 = 'Категорії';
$_['text_sort_order']           = 'Порядок';
$_['text_action_add_field']     = 'Додати&nbsp;поле';
$_['text_further']              = '...';
$_['text_record']               = 'Записи';
$_['text_what_records']         = 'Записи';
$_['text_comment']              = 'Коментарі';
$_['text_all_blogs']            = 'Всі категорії';
$_['text_further_more_ru']      = 'Подробнее&rarr;';
$_['text_further_more_en']      = 'More&rarr;';
$_['text_further_more_ua']      = 'Детальніше&rarr;'; // переменная добавлена только в языковой файл
$_['text_further_more']         = '&rarr;';

$_['button_add_module'] 		= 'Додати віджет у схеми';
$_['button_apply'] 				= 'Застосувати';
$_['button_delete'] 			= 'Видалити';
$_['sc_button_delete'] 			= 'Видалити';

$_['entry_docs'] 			    = 'Документація';
$_['entry_hidden_pro'] 			    = 'Сховати PRO параметри';
$_['entry_show_pro'] 			    = 'Показати PRO параметри';

$_['ok_delete_all_tables'] 			    = 'Всі таблиці модуля видалені';
$_['url_delete_all_tables_text'] 		= 'Видалити всі таблиці модуля!';
$_['url_delete_all_connect_text'] 		= 'Видалити всі підключення!';
$_['ok_delete_all_settings']    = 'Всі налаштування модуля видалені';
$_['url_delete_all_settings_text']    = 'Видалити всі налаштування модуля!';
$_['text_delete_settings_tables']    = 'Видалити всі налаштування таблиці модуля?';
$_['text_delete_settings_tables_hide']    = 'Приховати видалення налаштувань та таблиць модуля';

$_['entry_tab_options']   		= 'Налаштування';
$_['entry_tab_images']   		= 'Зображення';
$_['entry_tab_templates']   	= 'Шаблони';
$_['entry_tab_stat']   			= 'Статистика';
$_['entry_tab_access']   		= 'Доступи';

$_['entry_admin_visualeditor_status'] = 'Візуальний редактор<br> в записах <br> (адмін. частина)';
$_['entry_short_desc_trim_status'] = 'Застосувати правила "обрізання" <br>до стислого опису';

$_['text_widget_avatar']   		= 'Аватари користувача';
$_['text_what_latest']          = 'Даті';
$_['text_what_popular']         = 'Популярності';
$_['text_what_rating']          = 'Рейтингу';
$_['text_what_comments']        = 'Обговоренню';
$_['text_what_sort']            = 'За порядком';
$_['text_what_list']            = 'Шаблон';
$_['text_what_hook']            = 'Hook';
$_['text_what_rand']			= 'Випадкові';
$_['text_pointer_product_id']	= 'товари';
$_['text_pointer_category_id']	= 'категорії товарів';
$_['text_pointer_manufacturer_id']	= 'виробник товарів';
$_['text_pointer_record_id_date'] = 'перелінковка записів';

$_['entry_related_date_compare_str'] = 'Раніше / Пізніше';
$_['entry_related_date_compare_str_down'] = 'Раніше (<)';
$_['entry_related_date_compare_str_up'] = 'Пізніше (>)';


$_['entry_related_date_compare_main'] = 'Категорії: зв&#39язані / головна';
$_['entry_related_date_compare_main_down'] = 'Зв&#39язані';
$_['entry_related_date_compare_main_up'] = 'Головна';

$_['entry_cache_auto_clear'] = 'Автоматична очищення <br>всього кешу (в годинах)';

$_['text_pointer_blog_id']		= 'категорії блогу';
$_['text_what_asc']             = 'За зростанням';
$_['text_what_desc']            = 'За зменшенням';
$_['text_what_sort']           	= 'Порядку';
$_['text_what_date']           	= 'Даті';
$_['text_what_rating']         	= 'Рейтингу';
$_['text_what_rate']           	= 'Оцінці';
$_['text_layout_all']           = 'Всі';
$_['text_enabled']              = 'Включено';
$_['text_disabled']             = 'Виключено';
$_['text_what_reviews']         = 'За обговореннями';
$_['text_january']              = "січня";
$_['text_february']             = "лютого";
$_['text_march']                = "березня";
$_['text_april']                = "квітня";
$_['text_may']                  = "травня";
$_['text_june']                 = "червня";
$_['text_july']                 = "липня";
$_['text_august']               = "серпня";
$_['text_september']            = "вересня";
$_['text_october']              = "жовтня";
$_['text_november']             = "листопада";
$_['text_december']             = "грудня";
$_['text_today']                = "Сьогодні";
$_['text_date']            		= "d M Y";
$_['text_hours']           		= " H:i:s";
$_['text_default_store']		= 'Основний магазин';
$_['text_group_reg']			= 'Зареєстровані';
$_['text_group_order']			= 'Ті, що купили любий товар';
$_['text_group_order_this']		= 'Ті, що купили цей товар';
$_['text_layout_record']		= 'Record';
$_['text_layout_blog']			= 'Blog';
$_['text_url_for_module']		= 'ЧПУ для модуля';
$_['text_reviews_for_module']	= 'Відгуки';
$_['text_url_script_reviews_products']	= 'Включити відгуки для товарів модулем';
$_['text_url_script_reviews_records']	= 'Включити відгуки для записів модулем';
$_['text_ghost'] = 'Гість';
$_['entry_title_position']          = 'Розташування заголовка запису';
$_['text_title_position_before']          = 'Перед зображенням';
$_['text_title_position_after']          = 'Після зображення';

$_['entry_widget_order']          = 'Порядок віджета в списку';

$_['entry_signer_answer']          = 'Підписався на відповідь';
$_['entry_micro_status']          = 'Мікророзмітка';

$_['entry_short_desc_trim_status'] = 'Застосувати правила "обрізання" <br>до стислого опису записів';

$_['text_widget_not_found'] = '<p style="font-size: 200%; line-height: 200%;  color: red;">Віджет не знайдено<br>Видаліть його<br> скоріш за все він вже не використовується платформою за непотрібністю</p>';

$_['entry_tab_settings_default']				= 'Загальні налаштування';

$_['entry_attribute_groups_status']     = 'Показувати атрибути записів <br>в списку <br><span class="help">Увага: включення цієї функції збільшує навантаження на MySQL <br>рекомендується використовувати з повним кешуванням сторінок</span>';

$_['entry_modal_stat_status']		= 'Кнопка модальної формы';



$_['entry_stat_html'] = 'HTML код в статистиці';

$_['entry_stat_a_class'] = 'Класи кнопки';
$_['entry_stat_href_html'] = 'Текст кнопки <br>посилання на <br>всі відгуки запису';

$_['entry_block_width_templates_2'] = '2 колонки';
$_['entry_block_width_templates_3'] = '3 колонки';
$_['entry_block_width_templates_4'] = '4 колонки';
$_['entry_block_width_templates_5'] = '5 колонок';
$_['entry_value_templates_clear'] = 'Очистити';
$_['entry_current_value'] = 'Поточна величина';
$_['entry_block_width_templates'] = 'Кількість колонок';
$_['entry_block_width'] = 'Ширина блоку <br>у списку <br>(за замовчуванням 100% <br>- 1 колонка) <br><span class="help">в %, px, em</span>';


$_['entry_convertor_news']		= 'Конвертер записів модуля News';
$_['entry_convertor_shopstore'] = 'Конвертер новин з теми ShopStore';
$_['entry_convertor_information']		= 'Конвертер записів модуля Статті';
$_['entry_comment_signer_answer_require']		= 'Обов&#39язкове';
$_['entry_widget']				= 'Віджет';
$_['entry_blog_search']				= 'Категорія зв&#39язку з пошуком';
$_['entry_og']				= 'Розмітка Open Graph <br>в &lt;head&gt; <br><span class="help"><a href="http://ogp.me/" target="_blank">Open Graph</a></span>';
$_['entry_avatar']				= 'Аватари';
$_['entry_adapter']				= 'Адаптер файлів модуля під тему';
$_['entry_scripts']				= 'Сценарії';
$_['entry_css_dir']				= 'Розташування генерованого <br>seocms.css';

$_['cache_css_dir']				= 'Папка кешу';
$_['image_css_dir']				= 'Папка зображень';
$_['theme_css_dir']				= 'Папка css теми';

$_['entry_comp_url']				= 'Режим сумісності ЧПУ<br><span class="help">Не включайте, якщо ЧПУ модуля працюють</span>';
$_['entry_box_share_record']		= 'Код соціальних кнопок на сторінці запису';
$_['entry_box_share_blog']		= 'Код соціальних кнопок в списку записів <br><span class="help">Токени записів, що використовуються: <br>{TITLE} - заголовок запису в списку <br>{URL} - посилання запису в списку<br>{DESCRIPTION} - опис запису в списку</span>';
$_['entry_comment_status_language']		= 'Показувати відгуки <br>для всіх мов<br><span class="help">Якщо виключно, будуть показуватись відгуки <br>тільки для поточної мови</span>';

$_['css_ascp-list-title']	= 'Заголовок запису в списку категорії (.ascp-list-title)';
$_['css_ascp-list-title-widget'] = 'Заголовок запису в списку віджету (.ascp-list-title-widget)';

$_['css_ascp-list-title-color']	= 'Колір';
$_['css_ascp-list-title-size']	= 'Розмір шрифту';
$_['css_ascp-list-title-line']	= 'Висота лінії';
$_['css_ascp-list-title-decoration']	= 'Підкреслення';

$_['entry_menu_admin_status']	= 'Головне меню адмін. частини';

$_['entry_position_types']	= 'Позиції / Кастомні позиції';
$_['entry_position_controller']	= 'Контролер обробки';
$_['entry_position_name']	= 'Ім&#39я змінної виводу';
$_['entry_add_position_type']	= 'Додати, не стандартну,<br> що існує в opencart, <br>кастомну позицію';

$_['entry_box_begin']			= 'Віджети: блок обертки (початковий HTML код) <br><span class="help">Токени, що використовуються: <br>{TITLE} - заголовок блоку <br>{CMSWIDGET} - ID віджету блоку <br></span>';
$_['entry_box_end']				= 'Віджети: блок обертки (закриваючий HTML код) <br><span class="help">Токени, що використовуються: <br>{TITLE} - заголовок блоку <br>{CMSWIDGET} - ID віджету блоку <br></span>';

$_['entry_avatar_default']		= 'Аватар користувача <br>за замовчуванням';
$_['entry_avatar_admin']		= 'Аватар адміністратора <br>за замовчуванням';
$_['entry_avatar_reg']			= 'Аватар зареєстрованого <br>за замовчуванням';
$_['entry_avatar_buy']			= 'Аватар того, що купував в магазині <br>за замовчуванням';
$_['entry_avatar_buyproduct']	= 'Аватар того, що купив цей товар <br>за замовчуванням';
$_['entry_avatar_dimension']    = 'Розміри аватара користувача <span class="help">px</span>';
$_['text_image_manager']        = 'Менеджер зображень';
$_['text_browse']               = 'Огляд';
$_['text_clear']                = 'Видалити';

$_['text_upload_allowed']       = 'jpg, JPG, jpeg, gif, GIF, png, PNG';

$_['entry_ajax']       			= 'Ajax завантаження';
$_['entry_customer_groups_avatar']   	= "Групи, що мають права на <br>завантаження та зміну аватарів";
$_['entry_upload_allowed']      = 'Дозволені для завантаження <br />типи файлів:<br />Формати поділяються комою<br /><span class="help">Дозволені для завантаження на сервер формати файлів. Формати поділяються комою.</span>';
$_['entry_doc_ready']			= "Відкладене завантаження JS коду<br> поля прив&#39язки<br><span class='help'>після завантаження <br>всього документу</span>";
$_['entry_format_date']     	= "Формат дати";
$_['entry_format_hours']     	= "Формат часу";
$_['entry_format_time']     	= "Синоніми дати та часу";
$_['entry_complete_status']     = 'Статус того, що купив товар:<br /><span class="help">Статус замовлення, при якому покупець <br>отримує статус такого, що купив цей товар</span>';
$_['entry_customer_groups']		= 'Групи покупців:';
$_['entry_store']				= 'Магазини:';
$_['entry_category_button']		= 'Кнопка категорій <br><span class="help">приклад: "Всі новини"</span>';
$_['entry_id_widget']			= 'ID віджету<span class="help">Використовуються як префікс id блоку віджету <br> та як змінна JS в цьому блоці cmswidget <br>блок віджет має конструкцію   <br>id="cmswidget-< ?php echo $cmswidget; ?>"</span>';
$_['entry_layout_url_status']   = 'В схемах URL шаблон<br/><span class="help">Якщо опцію включено, буде відбуватись пошук словосполучення в URL, <br>а не точна відповідність URL</span>';

$_['entry_url']					= 'URL';
$_['entry_url_schemes']			= 'URL:&nbsp;&rarr;';
$_['entry_url_template'] 		= '<br/><span class="help">без слеша на початку URL</span>&darr;&nbsp;шаблон по словосполученню в URL';

$_['entry_description_status']  = 'Стислий опис';
$_['entry_avatar_status']       = 'Аватар';
$_['entry_widget_pagination']	= 'Пагінація';
$_['entry_widget_sort']			= 'Порядок';
$_['entry_widget_limit']		= 'Ліміт';
$_['entry_category_status']     = 'Показувати категорію';
$_['entry_colorbox_theme']      = 'Colorbox тема<br/><span class="help">Тема jquery plugin colorbox для перегляду зображень.<br>Темы можна додавати та змінювати в папці<br>' . HTTPS_CATALOG . 'catalog/view/javascript/blog/colorbox/css</span>';
$_['entry_colorbox_disable']    = 'Примусово відключити Colorbox модуля SEO CMS';


$_['entry_cache_widgets']       = 'Повне кешування віджетів<br/><span class="help">При повному кешуванні віджетів швидкість обробки та виводу зростає в 2-5 раз, в залежності від кількості віджетів, що використовуються на сторінці</span>';
$_['entry_cache_pages']       	= 'Повне кешування сторінок<br/><span class="help">При повному кешуванні сторінок швидкість обробки та виводу зростає в багато разів, за рахунок того, що кешується вся сторінка</span>';

$_['entry_reserved']            = 'Зарезервовано';
$_['entry_pointer']             = 'Пов&#39язані';
$_['entry_records']             = 'Записи';
$_['entry_comment_must']		= 'Обов&#39язкове поле відгуку';
$_['entry_fields']             	= 'Редактор полів';
$_['entry_service']             = 'Сервіс';
$_['entry_end_url_record']  	= 'Завершення url для записів<br><span class="help">Якщо поставити . (крапка) або пробіл <br>то при встановленому параметрі в категорії "Закривати слешем", <br>записи слешем закриватись не будуть</span>';
$_['entry_widget_cached']  		= 'Кешувати віджет<br><span class="help">Кешування HTML коду віджета <br>Інколи кешувати віджет не потрібно, <br>якщо у вас в шаблонах є логіка додавання <br>JS скриптів та CSS стилів в документ</span>';
$_['entry_review_visual']       = 'Включити візуальний редактор <br>в адмін частині для відгуків';
$_['entry_order_ad']            = 'Сортувати записи за';
$_['entry_order']            	= 'Порядок сортування записів';
$_['entry_resize']            	= 'Адаптивний resize<br><span class="help">Після змінення стану параметра необхідно <br>очистити кеш зображень в папці /image/cache</span> ';
$_['entry_get_pagination']     	= 'GET змінна для пагінації віджетів<br><span class="help">Обережно, в деяких версіях <b>сторонніх</b> <br>seo модулів вони видаляють всі змінні <br>з GET окрім "своїх" та <b>tracking</b> (що не коректно), <br>для url и схем, які вони "обробляють"</span>';
$_['entry_karma_reg']			= 'За карму дозволено голосувати тільки <br>зареєстрованим користувачам';
$_['entry_comments_email']		= 'Надсилати копії <br>нових відгуків на e-mail &rarr;<br><span class="help">можна використовувати декілька e-mail через ;</span>';
$_['entry_admin_name']			= 'Виділяти відповіді <br>як адміністратор від &rarr;<br><span class="help">можна використовувати декілька імен через ;</span>';
$_['entry_admin_color']			= 'Колір заднього плану для відповіді<br><span class="help">від обраних імен</span>';

$_['entry_admins'] = 'Адміністратор';
$_['entry_admins_list'] = 'Перелік адміністраторів<span class="help">обирається зі списку покупців</span>';
$_['entry_date_status'] = 'Дата відгуку';

$_['entry_langfile']			= 'Кастомний мовний файл<br><span class="help">формат: <b>папка/файл</b> без розширення</span>';
$_['entry_record']			 	= 'Запис';
$_['entry_anchor']			 	= 'Прив&#39язка (JavaScript)<br><span class="help">За допомогою поля можна переміщати віджет <br>в любе місце сайту, змінювати вміст <br>блоку віджета, застосовуючи код jquery <br>$(\'<b>селектор блока</b>\') .html(data) .append(data) .prepend(data) <br> де змінна JS <br>data - розрахований результат блока <br>cmswidget - id віджета <br>prefix - код префікса <br><a href="https://www.google.com/search?q=jquery+селекторы+перемещение" target="_blank">Про синтаксис поля</a></span>';
$_['entry_handler']			 	= 'Обробник<br><span class="help">JavaScript (jquery) код обробки форми <br>після вдалого заповнення та виконання <br>наприклад: для закриття модального вікна colorbox <br>$.colorbox.close();</span>';

$_['entry_stat_color_background'] = 'Колір фона';
$_['entry_stat_color_main'] = 'Основний колір';
$_['entry_stat_color_two'] = 'Другий колір';
$_['entry_stat_color_three'] = 'Третій колір';
$_['entry_stat_color_four'] = 'Четвертий колір';
$_['entry_stat_color_five'] = 'П&#39ятий колір';


$_['entry_admin_id_view']     	= 'Показувати ID віджетів <br>в адмін частині';
$_['entry_widgets_show_hide']  	= 'Показувати / Приховати віджети';
$_['entry_widgets_show_all']  	= 'Показувати всі віджети';
$_['entry_css_min']  			= 'Мініфікація seocms.css';


$_['entry_buyer_status']     	= 'Статус "Купив товар"';
$_['entry_karma']     			= 'Карма';
$_['entry_comment_rating']      = 'Рейтинг';
$_['entry_rating']      		= 'Рейтинг';
$_['entry_rating_must']      	= 'Обов&#39язкова оцінка рейтингу';
$_['entry_visual_rating']      	= 'Візуальний рейтинг';
$_['entry_comment_rating_num'] 	= 'Ліміт голосування карми в записі<br><span class="help">Даний параметр потрібен для створення <br>опитів та голосувань. Поставте 1 для них</span>';
$_['entry_layout']              = 'Схеми:';
$_['entry_order_records']       = 'Сортування переліку записів';
$_['entry_html']                = 'Html';
$_['entry_position']            = 'Розміщення:';
$_['entry_status']              = 'Статус:';
$_['entry_sort_order']          = 'Порядок:';
$_['entry_blog']                = 'Категорії';
$_['entry_title_list_latest']   = 'Заголовок';
$_['entry_template']            = 'Шаблон';
$_['entry_template_comment']    = 'Шаблон "дерева" відгуків';
$_['entry_template_comment_stat'] = 'Шаблон статистики відгуків';
$_['entry_template_comments']   = 'Шаблон обертки <br><span class="help">Шаблон контейнера, <br>пріоритет шаблонів для записів,<br>в категорії таб "Налаштування", поле <br>"Шаблон виводу коментарів до запису"</span>';
$_['entry_number_per_widget']   = 'Кількість виведених записів';
$_['entry_what']                = 'what';
$_['entry_editor']              = 'Візуальний редактор (вкл./викл.)';
$_['entry_small_dim']           = 'Розмір зображень списку категорій';
$_['entry_big_dim']             = 'Розмір великого зображення в категорії';
$_['entry_avatar_dim']          = 'Розмір зображення в переліку';
$_['entry_blog_num_comments']   = 'Кількість відгуків на сторінці';
$_['entry_blog_num_records']    = 'Кількість записів на сторінці';
$_['entry_blog_num_desc']       = 'Кількість символів в описі';
$_['entry_blog_num_desc_words'] = 'Кількість слів в описі';
$_['entry_blog_num_desc_pred']  = 'Кількість речень в описі';
$_['entry_title_further']       = 'HTML код кнопки "детальніше/далі"';
$_['entry_install_update']      = 'Установка та оновлення';
$_['entry_comment_status_reg']  = 'Залишати відгуки можуть <br>тільки зареєстровані користувачі';
$_['entry_categories']          = 'Категорії товарів';
$_['entry_images_view']  		= 'Показувати зображення галереї в переліку записів';
$_['entry_images_dim']  		= 'Розміри зображень галереї в переліку';
$_['entry_comment_status']      = 'Коментувати?';
$_['entry_comment_status_now']  = 'Публікувати одразу';
$_['entry_view_date']           = 'Показувати дату';
$_['entry_view_share']          = 'Показувати share ';
$_['entry_view_avatar']         = 'Показувати зображення';

$_['entry_images_number']         = 'Кількість зображень галереї';
$_['entry_images_number_hide']    = 'Показувати в модальному вікні <br>приховані зображення галереї <br> (обмежено кількістю) ';

$_['entry_images_position']       = 'Позиція зображень галереї';

$_['entry_avatar_adaptive_resize']       = 'Адаптивний ресайз <br>зображень переліку записів';
$_['entry_images_adaptive_resize']       = 'Адаптивний ресайз <br>зображень галереї';


$_['text_images_position_after']  = 'За';
$_['text_images_position_before'] = 'Під';

$_['entry_view_record']         = 'Показувати посилання запису';
$_['entry_view_comments']       = 'Показувати кількість коментарів';
$_['entry_view_viewed']         = 'Показувати кількість переглядів';
$_['entry_view_rating']         = 'Показувати рейтинг';
$_['entry_view_rate']           = 'Показувати рейтинг відгуку';
$_['entry_view_author']         = 'Показувати автора';
$_['entry_categories']          = 'Категорії товарів';
$_['entry_name_field']          = 'Назва поля<br/><span class="help">Має бути унікальним на всю систему <br>і тільки англійськими літерами,<br>ставте префікс (наприклад af_blog_plus), <br>для того щоб назва поля була унікальною</span>';
$_['entry_view_captcha']        = 'Captcha';
$_['entry_visual_editor']       = 'Візуальний редактор BB кодів';
$_['entry_bbwidth']             = 'Ширина зображення<br><span class="help">що вставляється через візуальний редактор BB кодів (в px)</span>';
$_['entry_comment_signer']      = 'Підписка';
$_['entry_comment_signer_answer']      = 'Підписка на відгуки';
$_['entry_stat_status']			 = 'Статистика рейтингів';
$_['entry_pagination']      	= 'Пагінація';
$_['entry_fields_view']         = 'Показати відразу додаткові поля';
$_['entry_date_added']          = 'Дата:';
$_['entry_widget_counting']		= 'Включити підрахунок записів в категорії';

$_['tab_general']               = 'Схеми';
$_['tab_list']                  = 'Віджети';
$_['tab_options']               = 'Налаштування';
$_['tab_color']               	= 'CSS сайту';

$_['button_add_list']           = 'Додати віджет';
$_['button_update']           	= 'Змінити';

$_['button_clone_widget']       = 'Копіювати віджет';
$_['button_continue']           = "Далі";
$_['button_help']	            = "Допомога";
$_['button_pro']	            = "Розширенні налаштування";

$_['column_blog']               = 'Категорія';

$_['error_delete_old_settings'] = '<div style="color: red; text-align: left; text-decoration: none;">Наразі не можна видаляти налаштування старих версій<br><ins style="text-align: left; text-decoration: none; font-size: 13px; color: red;">(перезбережіть "налаштування", "схеми" та "віджети", <br>тільки після цього натисніть цю кнопку)</ins></div>';
$_['error_permission']          = 'У вас немає прав для зміни налаштувань модуля!';

$_['error_addfields_name']      = 'Невірно вказано назву додаткового поля';

$_['access_777']                = 'Не встановлено права на файл<br>Встановіть права 777 на цей файл вручну.';
$_['remove_777']                = 'Підключення видалене';
$_['ok_777']   		            = 'Підключення встановлене';
$_['ok_create_tables']          = 'Дані оновлено';
$_['hook_not_delete']           = 'Дану схему не можна видаляти, вона необхідна для сервісних функцій модуля (seo)<br>В разі якщо ви випадково видалили, додайте таку ж саму схему з такими саме параметрами<br>';
$_['type_list']                 = 'Віджет:';


$_['entry_about'] 			    = 'Про модуль';
$_['text_about']              	= <<<EOF

EOF;

$_['entry_faq'] 			    = 'FAQ';
$_['text_faq']              	= <<<EOF

EOF;



$_['url_fields'] 	=  'index.php?route=catalog/fields&token=';
$_['url_schemes'] 	=  'index.php?route=module/blog/schemes&token=';
$_['url_widgets'] 	=  'index.php?route=module/blog/widgets&token=';
$_['entry_fields_editor'] 	=  'Редактор полів';
$_['url_dev_oa'] 	=  'http://opencartadmin.com/index.php?route=account/download';
$_['url_diler_of']  = 'https://opencartforum.com/files/file/2371-oc-2-seo-cms-pro-2-блог-новости-отзывы-галерея-формы/';
$_['url_diler_lo']  = 'http://liveopencart.ru/opencart-moduli-shablony/moduli/novosti-stati/oc-2-seo-cms-pro-2-blog-novosti-otzyivyi-galereya-formyi';
$_['url_buy'] 	 	= 'http://opencartadmin.com/seo-cms-pro-2.html';
$_['url_buy_text']	= 'Купити';

$_['url_dev_oa_text'] 	 = 'У розробника: <br><span style="color: #93FF93; font-weight: bold; font-size: 15px;">opencartadmin.com</span>';
$_['url_diler_of_text']  = 'У дилера: <br><span style="color: #93FF93; font-weight: bold; font-size: 15px;">opencartforum.com</span>';
$_['url_diler_lo_text']  = 'У дилера: <br><span style="color: #93FF93; font-weight: bold; font-size: 15px;">liveopencart.ru</span>';

$_['text_upgrade_5_6']	= '<div style="margin-left: 20px; margin-bottom: 20px;"><a href="http://opencartadmin.com/faq-seo-cms-pro-kak-obnovit-s-versii-5-do-versii-6.html" target="_blank">Інструкція з оновлення на нову версію</a></div>';

$_['css_help']         = '<span style="color: #008000; font-size: 15px;">Для початку дії цих налаштувань, якщо ви не робили ніяких змін в файлі <br>/catalog/view/theme/default/stylesheet/<span style="color: #7D00A6; font-weight: bold; font-size: 15px;">blog.css</span> , <br>видаліть цей файл, але якщо цей файл знаходиться в папці вашої теми, <br>та ви не робили в ньому жодних змін - видаліть його також <br><span style="color: #919191; font-size: 15px;">(налаштування в кожній версії будуть додаватись)</span></span>';
$_['css_user'] = 'Кастомний css код';

$_['css_background_body']     = 'Фоновий колір підкладки';
$_['css_record-content']     = 'Під "тілом" запису';
$_['css_blog-content']       = 'Під "тілом" переліку записів в категорії';
$_['css_ascp-list-title-weight']	= 'Насиченість шрифту';

$_['text_rdda'] = 'ip';
$_['text_tsop'] = 'POST';

if (isset($_SERVER['REMOTE_ADDR']))
$_['value_rdda'] = $_SERVER['REMOTE_ADDR'];
else
$_['value_rdda'] = '';

if (isset($_SERVER['HTTP_USER_AGENT']))
$_['value_tnega'] = $_SERVER['HTTP_USER_AGENT'];
else
$_['value_tnega'] = '';


if ((isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on' || $_SERVER['HTTPS'] == '1')) || (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && (strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) == 'https') || (!empty($_SERVER['HTTP_X_FORWARDED_SSL']) && strtolower($_SERVER['HTTP_X_FORWARDED_SSL']) == 'on')))  {
	$_['value_revres'] = HTTPS_SERVER;
	$_['value_ctlg'] = HTTPS_CATALOG;
} else {
	$_['value_revres'] = HTTP_SERVER;
	$_['value_ctlg'] = HTTP_CATALOG;
}

$_['text_ctlg'] = 'catalog';
$_['text_gnal'] = 'lang';
$_['text_liame'] = 'email';
$_['text_ptth'] = 'http';
$_['text_dohtem'] = 'method';
$_['text_rev'] = 'ver';
$_['text_rev_model'] = 'model';
$_['text_rev_knil'] = "http://opencartadmin.com/index.php?route=record/ver";
$_['sc_ocas'] = "http://opencartadmin.com/index.php?route=record/ver";
$_['text_fgcont'] = 'file_get_contents';
$_['text_yreuq_dliub_ptth'] = 'http_build_query';
$_['text_redaeh'] = 'header';
$_['text_tnetnoc'] = 'content';
$_['text_liame_gifnoc'] = 'config_email';
$_['text_strconc'] = 'stream_context_create';
$_['text_edom_efas'] = 'safe_mode';
$_['text_teg_ini'] = 'ini_get';
$_['text_redaeh_stpo_1'] = "User-Agent: ";
$_['text_redaeh_stpo_2'] = " \r\n";
$_['text_redaeh_stpo_3'] = "Content-type: application/x-www-form-urlencoded\r\n";
$_['text_redaeh_stpo_4'] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8 \r\n";
$_['text_redaeh_stpo_5'] = "";
$_['text_redaeh_stpo_6'] = "";
$_['text_redaeh_stpo_7'] = "";
$_['text_redaeh_stpo_8'] = "";
$_['text_redaeh_stpo_9'] = "Referer:";
$_['text_redaeh_stpo_10'] = "\r\n";


$_['entry_comment_types'] = "Типи коментарів / відгуків";
$_['entry_id'] = "ID";
$_['entry_title'] = "Заголовок";
$_['entry_add_comment_type'] = "Додати тип";


$_['entry_multifile_ext'] = "Пакетне завантаження зображень, <br>дозволені типи файлів";
$_['entry_multifile_size'] = "Пакетне завантаження зображень, <br>максимальний розмір файлу";
$_['entry_multifile_num'] = "Пакетне завантаження зображень, <br>максимальна кількість файлів";


$_['entry_widget_status'] = "Статус";

$_['entry_tab_fields'] = "Поля";
$_['entry_url_sitemap'] = "SEO CMS Sitemap";

$_['entry_tab_modal'] = "Модальне";
$_['entry_modal_status'] = 'Вивід в модальне вікно <br><span class="help">Вміст буде виведено в модальне вікно <br>Заголовок посилання - назва віджету</span>';
$_['entry_modal_a_class'] = 'CSS класи посилання виклику <br>модального вікна <br><span class="help">Заголовок посилання - назва віджету</span>';

$_['entry_modal_cb_width'] = 'Ширина модального вікна <br>(width) <br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_height'] = 'Висота модального вікна <br>(height) <br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';

$_['entry_modal_cb_maxWidth'] = 'Максимальна ширина <br>модального вікна <br>(maxWidth) <br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_maxHeight'] = 'Максимальна висота <br>модального вікна <br>(maxHeight) <br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_title'] = 'Заголовок <br>модального вікна <br><span class="help">Заголовок - назва віджета</span>';
$_['entry_modal_cb_opacity'] = 'Прозорість фону <br>модального вікна <br><span class="help">Величини: <br>0 (максимальна прозорість) <br>1 (мінімальна прозорість) <br>0.5 - прозорість 50%</span>';

$_['entry_modal_cb_innerWidth'] = 'Ширина модального вікна <br>(innerWidth) <br>Альтернативний параметр <br>виключає границі та кнопки<br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_innerHeight'] = 'Висота модального вікна <br>(innerHeight) <br>Альтернативний параметр <br>виключає границі та кнопки<br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';

$_['entry_modal_cb_initialWidth'] = 'Початкова ширина <br>модального вікна <br>до завантаження контенту <br>(initialWidth) <br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_initialHeight'] = 'Початкова висота <br>модального вікна <br>до завантаження контенту <br>(initialHeight) <br><span class="help">Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';

$_['entry_modal_cb_top'] = 'Вертикальне <br>позиціонування зверху <br>(top)<span class="help">замість використання, <br>за замовчуванням, позиції <br>центрування <br>в області перегляду <br>Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_bottom'] = 'Вертикальне <br>позиціонування знизу <br>(bottom)<span class="help">замість використання, <br>за замовчуванням, позиції <br>центрування <br>в області перегляду <br>Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';

$_['entry_modal_cb_left'] = 'Горизонтальне <br>позиціонування ліворуч <br>(left)<span class="help">замість використання, <br>за замовчуванням, позиції <br>центрування <br>в області перегляду <br>Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_right'] = 'Горизонтальне <br>позиціонування праворуч <br>(right)<span class="help">замість використання, <br>за замовчуванням, позиції <br>центрування <br>в області перегляду <br>Величини: %, px (100%, 100px, 100)</span><span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';


$_['entry_modal_cbmobile_width_browser'] = 'Ширина вікна браузера <br>при якому вважати його <br>мобільним <br><span class="help">Величина в px, вле без вказання px та % <br>Любе значення вмикає перевірку <br>мобільного  пристрою або <br>мобільного браузера </span>';
$_['entry_modal_cbmobile'] = 'при мобільному';

$_['entry_modal_cb_fixed'] = 'Відображення у фіксованому <br>положенні в області <br>перегляду відвідувача <br>(fixed)<span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_reposition'] = 'Переміщує модальне вікно <br>при зміненні розміру вікна <br>браузера <br>(reposition)<span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_scrolling'] = 'Полоси прокрутки <br>для вмісту <br>(scrolling)<span class="help"><a href="http://www.jacklmoore.com/colorbox/" target="_blank">Документація Colorbox</a></span>';
$_['entry_modal_cb_resize'] = 'Ресайз модального вікна, <br>після його завантаження, <br> згідно вмісту';
$_['entry_modal_html'] = 'HTML код, при натисканні <br>на який <br>спрацьовує <br>виклик модального вікна<span class="help">Якщо поле не заповнене, таким кодом рахується назва віджету. <br>PHP код в цьому полі не виконується</span>';

$_['entry_tab_slider'] = 'Слайдер';
$_['entry_widget_slider'] = 'Статус слайдера';

$_['entry_cache_expire'] = 'Час життя кеш файлу <br>модуля (в секундах)';
$_['entry_cache_max_files'] = 'Максимальна кількість файлів <br>в папці кешу модуля';
$_['entry_cache_maxfile_length'] = 'Максимальний розмір <br>кеш файлу модуля (в байтах)';



$_['entry_widget_render'] = 'Шаблонізатор';
$_['text_widget_render_template'] = 'PHP .tpl';
$_['text_widget_render_twig'] = 'TWIG .twig';

$_['entry_seocmscss_status'] = 'Статус seocms.css';


$_['url_schemes_text'] = 'Схеми';
$_['url_widgets_text'] = 'Віджети';
$_['url_tp_link'] = 'https://opencartadmin.com/support.html';

$_['text_ocmod_refresh'] = 'Оновити модифікатори';
$_['text_refresh_ocmod_success']= 'Вдало';
$_['text_ocmod_refresh_fail'] = 'Помилка';

$_['text_url_cache_remove'] 	= 'Видалити кеш';
$_['text_cache_remove_success']	= 'Виконано вдало';
$_['text_cache_remove_fail'] 	= 'Не вийшло видалити';
$_['text_brand'] = 'Brand';

$_['text_check_ver'] = '<div style="text-align: center; text-decoration: none;">Перевірити нову версію</div>';

$_['text_server_date_state'] = 'Станом на';
$_['text_current_version_text'] = '<div style="color: #306793;">Ваша поточна версія</div>';
$_['text_last_version_text'] = '<div style="color: #306793;">Остання версія</div>';
$_['text_update_yes'] = '<div style="color: red;">Рекомендуєтся оновити модуль</div>';
$_['text_update_no'] = '<div style="color: green;">Оновлення не потрібне, у вас сама остання версія модуля</div>';

$_['text_loading']              = '<div style=&#92;\'padding-left: 30%; padding-top: 10%; font-size: 21px; color: #008000;&#92;\'>Завантажується...<i class=&#92;\'fa fa-refresh fa-spin&#92;\'></i></div>';
$_['text_loading_adapter']      = '<div style="font-size: 14px; color: #000;">Завантажується...<i class="fa fa-refresh fa-spin"></i></div>';
$_['text_loading_main']         = '<div style=&#92;\'font-size: 14px; color: #000;&#92;\'>Завантажується...<i class=&#92;\'fa fa-refresh fa-spin&#92;\'></i></div>';

$_['text_error_server_connect'] = 'Помилка з&#39єднання з сервером';



require_once(DIR_SYSTEM . 'helper/seocmsprofunc.php');
$_['blog_version_model'] = $_['sc_model'] = seocms_checkmodel();



