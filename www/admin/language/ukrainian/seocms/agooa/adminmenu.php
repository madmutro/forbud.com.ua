<?php
$_['agoo_menu_modules'] = 'Модулі';
$_['agoo_menu_options'] = 'Головна модуля';
$_['agoo_menu_layouts'] = 'Схеми';
$_['agoo_menu_widgets'] = 'Віджети';
$_['agoo_menu_categories'] = 'Категорії';
$_['agoo_menu_records'] = 'Записи';
$_['agoo_menu_comments'] = 'Коментарі';
$_['agoo_menu_reviews'] = 'Відгуки';
$_['agoo_menu_adapter'] = 'Адаптер шаблонів модуля під тему';
$_['agoo_menu_fields'] = 'Кастомні поля форм';
$_['agoo_menu_sitemap'] = 'Sitemap';

?>