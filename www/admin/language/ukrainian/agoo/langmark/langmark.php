<?php
include('version.php');

$_['url_module_text'] = 'SEO мультимова-мультирегіон';

$_['ocmod_langmark_name'] = $_['url_module_text'] ;
$_['ocmod_langmark_version'] = $_['langmark_version'] ;
$_['ocmod_langmark_mod'] = 'langmark';
$_['ocmod_langmark_author'] = 'opencartadmin.com';
$_['ocmod_langmark_link'] = 'https://opencartadmin.com';
$_['ocmod_langmark_html'] = 'Модифікатор для '.$_['url_module_text'].' успішно встановлений';



$_['text_widget_langmark'] = $_['url_module_text'] . ' ver.' . $_['text_widget_langmark_version'];
$_['widget_langmark_version'] = $_['text_widget_langmark_version'];
$_['text_mod_add_langmark'] = '';
$_['text_widget_langmark_settings'] = $_['text_widget_langmark'];

$_['order_langmark'] = '0';

$_['text_separator'] = ' > ';

$_['entry_langmark_widget_status']          = 'Статус модуля';
$_['entry_langmark_widget_status_scripts']  = 'Скрипти (scripts js) <br>тіла переліку товарів';
$_['entry_langmark_widget_content']         = 'CSS селектор тіла переліку товарів';
$_['entry_langmark_widget_breadcrumb']      = 'CSS селектор хлібних крихт';
$_['entry_langmark_widget_h1']          	= 'CSS селектор мета тегу H1';
$_['entry_langmark_widget_install_success']	= 'Таблиця віджета Мітки товарів встановлена<br>';
$_['entry_langmark_widget_install']			= 'Підключення віджета SEO мультимова - успішно<br>';
$_['entry_langmark_widget_types']			= 'Елементи, що будуть видалені <br>з шаблону';
$_['entry_number']							= 'Номер';
$_['entry_add_langmark_widget_type']		= 'Додати елемент';
$_['entry_url_langmark']		= 'Сторінка налаштувань модуля';


$_['entry_anchor_templates'] = 'Шаблони прив&#39язки';
$_['entry_anchor_value'] = 'Поточне значення';
$_['entry_anchor_templates_clear'] = 'Очистити';

$_['entry_anchor_templates_tab'] = 'У вкладці (за замовчуванням)';
$_['entry_box_begin_templates'] = 'Блок (початковий HTML код) шаблони';
$_['entry_box_end_templates'] = 'Блок (закриваючий HTML-код) шаблони';
$_['entry_box_begin_templates_tab'] = 'Блок (початковий HTML код) шаблон у вкладці (за замовчуванням)';
$_['entry_box_end_templates_tab'] = 'Блок (закриваючий HTML-код) шаблон у вкладці (за замовчуванням)';
$_['entry_box_begin_templates_empty'] = 'Блок (початковий HTML код) шаблон пустий блок (за замовчуванням)';
$_['entry_box_end_templates_empty'] = 'Блок (закриваючий HTML-код) шаблон пустий блок (за замовчуванням)';
$_['entry_box_begin_value'] = 'Поточне значення';
$_['entry_box_end_value'] = 'Поточне значення';

$_['entry_anchor_templates_html'] = 'html шаблон';
$_['entry_anchor_templates_prepend'] = 'prepend шаблон';
$_['entry_anchor_templates_append'] = 'append шаблон';
$_['entry_anchor_templates_before'] = 'before шаблон';
$_['entry_anchor_templates_after'] = 'after шаблон';
$_['text_anchor_templates_selector'] = 'ВВЕДІТЬ СЕЛЕКТОР TAG, #ID, .CLASS';

$_['text_adapter_edit'] = 'Адаптувати мультимову';
$_['entry_replace_text'] = 'Значення для заміни';
$_['entry_replace_text_na'] = 'на';


$_['entry_load_template'] = 'Завантажити зразок шаблону';
$_['entry_load_template_new'] = 'Завантажити змінений шаблон';

$_['html_help_adapter'] = <<<EOF
Приберіть зайві теги &lt;form ...&gt; &lt;/form&gt; &lt;input ...&gt;<br>
Додайте або змініть тегам &lt;a&gt; атрибут href, він повинен бути href="&lt;?php echo \$language[<strong style="color: green;">'url'</strong>]; ?&gt;"<br>
Знизу те, що знайшов AI та намагається замінити

EOF;
/*********************************************************************/

$_['url_back_text']             = 'В налаштування модуля';
$_['url_modules_text']          = 'В перелік модулів';

$_['tab_main']     				= 'Головна сторінка';
$_['entry_main_title']     		= 'Заголовок сторінки <br><span class="vhelp">Мета-тег: title</span>';
$_['entry_main_description']    = 'Опис сторінки <br><span class="vhelp">Мета-тег: description</span>';
$_['entry_main_keywords']     	= 'Ключові слова сторінки <br><span class="vhelp">Мета-тег: keywords</span>';

$_['tab_ex']     				= 'Винятки';
$_['entry_ex_multilang']     	= 'В маршрутизаторі <span class="help">(роздільник - переведення каретки PHP_EOL)</span>';
$_['entry_ex_multilang_route']  = 'Винятки для route';
$_['entry_ex_multilang_uri']    = 'Винятки для uri';
$_['entry_ex_url']     			= 'у формувальнику префіксів <span class="help">(роздільник - переведення каретки PHP_EOL)</span>';
$_['entry_ex_url_route']     	= 'Винятки для route';
$_['entry_ex_url_uri']     		= 'Винятки для uri';
$_['entry_add']     			= 'Додати';

$_['entry_url_close_slash']		= 'Закривати URL списку слешем "/"';

$_['entry_main_prefix_status']		= 'Не використовувати префікс мови (якщо він визначений) для головної сторінки. Решта сторінок будуть з префіксом<br>
<span class="vhelp">Включайте тільки в тому разі, якщо у вас для всіх "мов" встановлений префікс та ви бажаєте для якоїсь "мови" використовувати головну сторінку без префіксу</span>
';


$_['url_opencartadmin']         = 'http://opencartadmin.com';

$_['heading_title']             = ' <div style="height: 21px; margin-top:5px; text-decoration:none;"><ins style="height: 24px;"><img src="view/image/langmark-icon.png" style="height: 16px; margin-bottom: -3px; "></ins><ins style="margin-bottom: 0px; text-decoration:none; margin-left: 9px; font-size: 13px; font-weight: 600; color: green;">SEO мультимова-мультирегіон</ins></div>';
$_['heading_dev']               = 'Розробник <a href="mailto:admin@opencartadmin.com" target="_blank">opencartadmin.com</a><br>&copy; 2013-' . date('Y') . ' All Rights Reserved';

$_['text_pagination_title']     = 'page';
$_['text_pagination_title_russian'] = 'сторінка';
$_['text_pagination_title_ukraine'] = 'сторінка';

$_['text_widget_html']          = 'Мовний HTML, HTML вставка';
/*
$_['text_loading']              = "<div style=\'padding-left: 30%; padding-top: 10%; font-size: 21px; color: #008000;\'>Загружается...<img src=\'view/image/blog-icon-loading.gif\' style=\'height: 16px;\'><\/div>";
$_['text_loading_small'] 		= "<div style=\'font-size: 19px; padding: 5px; color: #008000;\'>Загружается...<img src=\'view/image/blog-icon-loading.gif\' style=\'height: 16px;\'></div>";
*/
$_['text_loading_small'] = '<div style=&#92;\'color: #008000;&#92;\'>Завантажується...<i class=&#92;\'fa fa-refresh fa-spin&#92;\'></i></div>';
$_['text_loading'] = '<div>Завантажується...<i class="fa fa-refresh fa-spin"></i></div>';
$_['text_loading_langmark'] = '<div>Завантажується...<i class="fa fa-refresh fa-spin"></i></div>';



$_['text_update_text'] = 'Натисніть на кнопку.<br>Ви оновили модуль';
$_['text_module']               = 'Модулі';
$_['text_add']                  = 'Додати';
$_['text_action']               = 'Дія:';
$_['text_success']              = 'Модуль оновлений';
$_['text_content_top']          = 'Вміст шапки';
$_['text_content_bottom']       = 'Вміст підвалу';
$_['text_column_left']          = 'Ліва колонка';
$_['text_column_right']         = 'Права колонка';
$_['text_what_lastest']         = 'Останні записи';
$_['text_select_all']           = 'Виділити все';
$_['text_unselect_all']         = 'Зняти виділення';
$_['text_sort_order']           = 'Порядок';
$_['text_further']              = '...';
$_['text_error'] 				= 'Помилка';
$_['text_layout_all']           = 'Всі';
$_['text_enabled']              = 'Включено';
$_['text_disabled']             = 'Відключено';
$_['text_multi_empty']          = 'Перейдіть у вкладку "Установка та оновлення" та натисніть кнопку "Створення та оновлення даних для модуля (при встановленні та оновленні модуля)"';

$_['entry_lang_default']          	= 'Мова за замовчуванням';

$_['entry_name']          		= 'Ім&#39я';
$_['entry_prefix']          	= 'Префікс мови';
$_['entry_prefix_main']        	= 'Головна мова';

$_['entry_seomore_code']        	= 'Код JS';

$_['entry_hreflang']          	= 'Мета тег hreflang';
$_['entry_hreflang_status']   	= 'Статус мета тегу hreflang ';
$_['entry_commonhome_status']   = '<span class="vhelp">Видалити з URL Головної</span> <br>index.php?route=common/home ';
$_['entry_languages']          	= 'Зв&#39язана мова';
$_['entry_access']        		= 'Доступ';

$_['entry_remove_description_status']          = 'Прибрати опис на <br>додаткових сторінках';
$_['entry_add_rule']          	= 'Додати правило';
$_['entry_title_template']    	= 'Ім&#39я файлу шаблону';
$_['entry_desc_types']    		= 'Правила, згідно з якими <br>в шаблонах (ім&#39я файлу)<br>буде прибраний опис <br>на додаткових <br>сторінках пагінації';

$_['entry_pagination']          = 'Пагінація';
$_['entry_jazz']          		= 'Підтримка ЧПУ формувальника Jazz';
$_['entry_pagination_prefix']   = 'Назва змінної пагінації';
$_['entry_title_pagination']    = 'Заголовок пагінації';
$_['entry_currencies']    		= 'Пов&#39язана валюта';

$_['entry_title_list_latest']   = 'Заголовок';
$_['entry_editor']              = 'Графічний редактор';
$_['entry_switch'] 			    = 'Включити модуль';
$_['entry_title_prefix'] 	    = 'Префікс мови<span class="help">Поставте префікс для мови,<br>наприклад для англійської мови <b>en</b><br>(url буде мати вигляд: http://site.com/en )<br>Якщо ви бажаєте щоб url з префіксом<br>завершувався слешем<br>(приклад: http://site.com/en/),<br>поставте префікс <b>en<ins style="color:green; text-decoration: none;">/</ins></b><br>або залиште поле <b>пустим</b><br>для мови <b>за замовчуванням</b></span>';
$_['entry_about'] 			    = 'Про модуль';
$_['entry_category_status']     = 'Показувати категорію';
$_['entry_cache_widgets']       = 'Повне кешування віджетів<br/><span class="help">При повному кешуванні віджетів <br>швидкість обробки та виведення зростає в 2-5 разів <br>в залежності від кількості віджетів <br>, що розташовані на сторінці</span>';
$_['entry_reserved']            = 'Зарезервовано';
$_['entry_service']             = 'Сервіс';
$_['entry_langfile']			= 'Кастомний файл мови<br><span class="help">формат: <b>папка/файл</b> без розширення</span>';
$_['entry_widget_cached']  		= 'Кешувати віджет<br><span class="help">Має більший пріоритет ніж повне кешування <br>всіх віджетів в загальних налаштуваннях, <br>інколи кешувати віджет не потрібно, <br>якщо у вас в шаблонах присутня логіка додавання <br>JS скриптів та CSS стилів в документ</span>';

$_['entry_anchor']			 	= '<b>Прив&#39язка</b><br><span class="help" style="line-height: 13px;">прив&#39язка до блоку через jquery<br>приклад для default теми opencart:<br>$(\'<b>#language</b>\').html(langmarkdata);<br>де langmarkdata (змінна javascript)<br>результат виконання html блоку</span>';


$_['entry_layout']              = 'Схеми:';

$_['entry_html']                = <<<EOF
<b>HTML, PHP, JS код</b><br><span class="help" style="line-height: 13px;">Доступне виконання PHP коду<br>
Змінні:<br>
\$languages - масив, що має структуру:<br>
 [код мови] => Array<br>
        (<br>
&nbsp;&nbsp;            [language_id] => id мови<br>
&nbsp;&nbsp;             [name] => ім&#39я мови<br>
&nbsp;&nbsp;             [code] => код мови<br>
&nbsp;&nbsp;             [locale] => locale мови<br>
&nbsp;&nbsp;             [image] => зображення мови<br>
&nbsp;&nbsp;             [directory] => папка<br>
&nbsp;&nbsp;             [filename] => ім&#39я файлу мови<br>
&nbsp;&nbsp;             [sort_order] => порядок<br>
&nbsp;&nbsp;             [status] => статус<br>
&nbsp;&nbsp;             [url] => url поточної сторінки для мови<br>
        )<br>
<br>
\$text_language - заголовок<br>
для мовного блоку
<br>
<br>
\$language_code - поточний код мови
<br>
\$language_prefix - поточний префікс мови
</span>
EOF;

$_['entry_position']            = 'Розташування:';
$_['entry_status']              = 'Статус:';
$_['entry_sort_order']          = 'Порядок:';

$_['entry_template']            = '<b>Шаблон</b>';
$_['entry_what']                = 'what';
$_['entry_install_update']      = 'Установка та оновлення';


$_['tab_general']               = 'Схеми';
$_['tab_list']                  = 'Віджети';
$_['tab_options']               = 'Налаштування';
$_['tab_pagination']            = 'Пагінація';

$_['button_add_list']           = 'Додати віджет';
$_['button_update']           	= 'Редагувати';
$_['button_clone_widget']       = 'Клонувати віджет';
$_['button_continue']           = "Далі";

$_['error_delete_old_settings'] = '<div style="color: red; text-align: left; text-decoration: none;">Наразі не можна видаляти налаштування старих версій<br><ins style="text-align: left; text-decoration: none; font-size: 13px; color: red;">(перезбережіть "налаштування", "схеми" та "віджети", <br>тільки після цього натисніть цю кнопку)</ins></div>';
$_['error_permission']          = 'У вас немає прав для зміни налаштувань модуля!';
$_['error_addfields_name']      = 'Невірно вказано назву додаткового поля';

$_['access_777']                = 'Не визначені права на файл<br>Встановіть права 777 на цей файл вручну.';
$_['text_install_ok']          = 'Дані оновлені';
$_['text_install_already']     = 'Дані присутні';
$_['hook_not_delete']           = 'Дану схему не можна видаляти, вона необхідна для сервісних функцій модуля (seo)<br>В разі якщо ви випадково видалили, добавте таку ж саму схему з такими ж параметрами<br>';
$_['type_list']                 = 'Віджет:';
$_['text_about']              	= <<<EOF

EOF;

$_['tab_other']                 = 'Інше';
$_['entry_two_status']          = 'Виправлення "подвійних слешів" в URL';

$_['entry_prefix_switcher']     = 'Виведення в перемикачі мов';
$_['entry_prefix_switcher_stores'] = 'Виведення в перемикачі мов всіх магазинів';
$_['entry_hreflang_switcher']   = 'Виведення в мета тег hreflang';

$_['entry_shortcodes']   = 'Шорткоди';

$_['entry_currency_switch']     = 'При зміненні мови примусово <br>переключати валюту згідно налаштувань, <br>незалежно від переключень валюти <br> користувачем на інших мовах <br>домену або піддомену';

$_['entry_use_link_status'] = 'Використовувати штатний <br>алгоритм формування ЧПУ';

$_['text_shortcodes_in'] = 'Шорткод для заміни';
$_['text_shortcodes_out'] = 'Заміна';
$_['text_shortcodes_action'] = 'Дія';
$_['url_create_text']           = '<div style="text-align: center; text-decoration: none;">Створення та оновлення<br>даних для модуля<br><ins style="text-align: center; text-decoration: none; font-size: 13px;">(при встановленні та оновленні модуля)</ins></div>';
$_['url_delete_text']           = '<div style="text-align: center; text-decoration: none;">Видалення всіх<br>налаштувань модуля<br><ins style="text-align: center; text-decoration: none; font-size: 13px;">(всі налаштування будуть видалені)</ins></div>';


$_['entry_copy_rules'] = 'Скопіювати правила';

$_['entry_store_id_related'] = 'Пов&#39язаний магазин';

$_['url_store_id_repated_text'] = '<div style="text-align: center; text-decoration: none;">Прив&#39язати всі категорії, товари,<br>виробників, інформаційні сторінки (статті)<br><ins style="text-align: center; text-decoration: none; font-size: 13px;">до цього магазину</ins></div>';

$_['entry_title_values'] = 'Змінні';
$_['entry_cache_diff'] = 'Роздільний кеш <br> (для регіонів поза мультімагазінами, <br> на одному магазині, з однаковими товарами)';

