<?php
$_['text_widget_html']          = 'HTML вставка';
$_['order_html'] = '100';
$_['text_separator']          = ' > ';
$_['entry_search']          = 'Форма пошуку';

$_['entry_anchor_templates'] = 'Шаблони прив&#39язки';
$_['entry_anchor_value'] = 'Поточне значення';
$_['entry_anchor_templates_clear'] = 'Очистити';

$_['entry_anchor_templates_tab'] = 'У вкладці (за замовчуванням)';
$_['entry_box_begin_templates'] = 'Блок (початковий HTML код) шаблони';
$_['entry_box_end_templates'] = 'Блок (закриваючий HTML-код) шаблони';
$_['entry_box_begin_templates_tab'] = 'Блок (початковий HTML код) шаблон у вкладці (за замовчуванням)';
$_['entry_box_end_templates_tab'] = 'Блок (закриваючий HTML-код) шаблон у вкладці (за замовчуванням)';
$_['entry_box_begin_templates_empty'] = 'Блок (початковий HTML код) шаблон пустий блок (за замовчуванням)';
$_['entry_box_end_templates_empty'] = 'Блок (закриваючий HTML-код) шаблон пустий блок (за замовчуванням)';
$_['entry_box_begin_value'] = 'Поточне значення';
$_['entry_box_end_value'] = 'Поточне значення';

$_['entry_anchor_templates_html'] = 'html шаблон';
$_['entry_anchor_templates_prepend'] = 'prepend шаблон';
$_['entry_anchor_templates_append'] = 'append шаблон';
$_['entry_anchor_templates_before'] = 'before шаблон';
$_['entry_anchor_templates_after'] = 'after шаблон';
$_['text_anchor_templates_selector'] = 'ВВЕДІТЬ СЕЛЕКТОР TAG, #ID, .CLASS';

$_['entry_tab_settings_cache'] 	= 'Кеш і модифікатори';
$_['entry_httpsfix_ocmod_refresh'] 	= 'Оновити кеш <br><span class="sc-color-clearcache">доповнень</span>';
$_['text_url_ocmod_refresh'] 	= 'Оновити';
$_['text_ocmod_refresh_success']= 'Кеш модифікаторів оновлено';
$_['text_ocmod_refresh_fail'] 	= 'Не вдалося оновити';

$_['entry_httpsfix_cache_remove'] 	= 'Видалити кеш <br><span class="sc-color-clearcache">файлів</span>';
$_['text_url_cache_remove'] 	= 'Видалити кеш';
$_['text_cache_remove_success']	= 'Кеш файлів очищений';
$_['text_cache_remove_fail'] 	= 'Не вдалося видалити';
$_['text_httpsfix_about'] 		= 'Про модуль';

$_['entry_httpsfix_cache_image_remove'] 	= 'Видалити кеш <br><span class="sc-color-clearcache">зображень</span>';
$_['text_url_cache_image_remove'] 	= 'Видалити кеш';
$_['text_cache_image_remove_success']	= 'Кеш зображень очищений';
$_['text_cache_image_remove_fail'] 	= 'Не вдалося видалити';

$_['entry_manufacturers']       = 'Виробники товарів';
$_['entry_products']       		= 'Товари';
