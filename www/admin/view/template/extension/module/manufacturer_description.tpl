<?php echo $header; ?>
<style type="text/css">
.toggle,.toggle:after,.toggle:before{-webkit-transition:all .2s cubic-bezier(.445,.05,.55,.95)}.addpad{padding-top:0;padding-bottom:0}.toggleWrapper{position:absolute;top:50%;overflow:hidden}.toggleWrapper input{position:absolute;left:-99em}.toggle{cursor:pointer;display:inline-block;position:relative;width:120px;height:30px;background:#ccc;border-radius:5px;transition:all .2s cubic-bezier(.445,.05,.55,.95)}.toggle:after,.toggle:before{position:absolute;line-height:30px;font-size:14px;z-index:2;transition:all .2s cubic-bezier(.445,.05,.55,.95)}.toggle:before{content:"OFF";left:20px;color:#ccc}.toggle:after{content:"ON";right:20px;color:#fff}.toggle__handler{display:inline-block;position:relative;z-index:1;background:#fff;width:65px;height:24px;border-radius:3px;top:3px;left:3px;-webkit-transition:all .2s cubic-bezier(.445,.05,.55,.95);transition:all .2s cubic-bezier(.445,.05,.55,.95);-webkit-transform:translateX(0);transform:translateX(0)}input:checked+.toggle{background:#66B317}input:checked+.toggle:before{color:#fff}input:checked+.toggle:after{color:#66B317}input:checked+.toggle .toggle__handler{width:54px;-webkit-transform:translateX(60px);transform:translateX(60px);border-color:#fff}
.adw + .form-group {
border-top: 1px solid #eee;
}
.form-horizontal .nav > li > a {
outline: none;
}
.form-horizontal .nav-tabs > li.active > a {
border-top: 2px solid #39B3D7 !important;
}
</style>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
		<span style="padding-right:20px;">
		<a href="https://opencartforum.com/profile/688391-alexdw/?do=content&type=downloads_file" target="_blank" data-toggle="tooltip" title="Другие дополнения" class="btn btn-info"><i class="fa fa-download"></i> Другие дополнения</a></span>
        <button type="submit" form="form-latest" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1>Manufacturer Description 1.20</h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-latest" class="form-horizontal">

          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $text_settings; ?></a></li>
            <li><a href="#tab-data" data-toggle="tab"><?php echo $entry_code; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">

          <div class="form-group">
            <label class="col-sm-2 col-xs-4 control-label"><?php echo $entry_status; ?></label>
            <div class="col-sm-4 col-xs-2">
			<div class="toggleWrapper">
                <?php if ($manufacturer_description_status) { ?>
				<input type="checkbox" id="input-status" name="manufacturer_description_status" value="1" checked="checked" />
                <?php } else { ?>
				<input type="checkbox" id="input-status" name="manufacturer_description_status" value="1" />
                <?php } ?>
			<label class="toggle" for="input-status"><span class="toggle__handler"></span>
			</label>
			</div>
			</div>
		  </div>

          <div class="form-group">
            <label class="col-sm-2 col-xs-4 control-label"><?php echo $entry_image; ?></label>
            <div class="col-sm-4 col-xs-2">
			<div class="toggleWrapper">
                <?php if ($manufacturer_description_image) { ?>
				<input type="checkbox" id="input-image" name="manufacturer_description_image" value="1" checked="checked" />
                <?php } else { ?>
				<input type="checkbox" id="input-image" name="manufacturer_description_image" value="1" />
                <?php } ?>
			<label class="toggle" for="input-image"><span class="toggle__handler"></span>
			</label>
			</div>
			</div>
		  </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $image_size; ?></label>
            <div class="col-sm-4"><div class="input-group"><span class="input-group-addon"><?php echo $entry_sizeh; ?></span>
              <input type="text" name="manufacturer_description_sizeh" value="<?php echo (isset($manufacturer_description_sizeh) ? $manufacturer_description_sizeh: '200') ; ?>" id="input-sizeh" class="form-control" />
			  <span class="input-group-addon"><?php echo $entry_sizew; ?></span>
              <input type="text" name="manufacturer_description_sizew" value="<?php echo (isset($manufacturer_description_sizew) ? $manufacturer_description_sizew: '200') ; ?>" id="input-sizew" class="form-control" />
            </div></div>
          </div>

              <div class="form-group">
                <label class="col-sm-2 control-label"><?php echo $entry_pos; ?></label>
                <div class="col-sm-4">
                  <select name="manufacturer_description_pos" id="input-pos" class="form-control">
                    <?php if ($manufacturer_description_pos) { ?>
                    <option value="0"><?php echo $text_before; ?></option>
                    <option value="1" selected="selected"><?php echo $text_after; ?></option>
                    <?php } else { ?>
                    <option value="0" selected="selected"><?php echo $text_before; ?></option>
                    <option value="1"><?php echo $text_after; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

<div class="adw alert alert-info">
	<p><?php echo $help_pos; ?></p>
</div>

          <div class="form-group">
            <label class="col-sm-2 col-xs-4 control-label"><?php echo $entry_temp; ?></label>
            <div class="col-sm-4 col-xs-2">
			<div class="toggleWrapper">
                <?php if ($manufacturer_description_temp) { ?>
				<input type="checkbox" id="input-temp" name="manufacturer_description_temp" value="1" checked="checked" />
                <?php } else { ?>
				<input type="checkbox" id="input-temp" name="manufacturer_description_temp" value="1" />
                <?php } ?>
			<label class="toggle" for="input-temp"><span class="toggle__handler"></span>
			</label>
			</div>
			</div>
		  </div>

<div class="adw alert alert-info">
	<p><?php echo $help_temp; ?></p>
</div>

			</div>

            <div class="tab-pane" id="tab-data">
          <div class="form-group">
			<label class="col-sm-2 control-label" for="input-limit"><?php echo $entry_code; ?></label>
			<div class="col-sm-10">
				<input type="text" name="manufacturer_description_code" value="<?php echo $manufacturer_description_code; ?>" placeholder="<?php echo $entry_code; ?>" id="input-code" class="form-control" />
            </div>
          </div>
          <div class="form-group">
			<label class="col-sm-12" ><?php echo $text_server; ?> <?php echo $server." - ".$is; ?></label>
			<label class="col-sm-12" ><?php echo $text_host; ?> <?php echo $host." - ".$ih; ?></label>
          </div>
			</div>
		  </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>