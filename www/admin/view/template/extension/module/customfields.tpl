<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-cfilds" data-toggle="tooltip" title="Сохранить" class="btn btn-primary" onclick="$('#form-cfilds').attr('action','<?php echo $save; ?>');"><i class="fa fa-save"></i></button>
        <button type="submit" form="form-cfilds" data-toggle="tooltip" title="Применить" class="btn btn-success" onclick="$('#form-cfilds').attr('action','<?php echo $apply; ?>');"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Отменить" class="btn btn-default"><i class="fa fa-reply"></i></a>
	</div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i>CustomFields</h3>
      </div>
      <div class="panel-body">
        <form action="" method="post" enctype="multipart/form-data" id="form-cfilds" class="form-horizontal">
		   <?php	$i = 1; ?>
		   <div class="form-group">
		   <style>
			.table thead > tr > td, .table tbody > tr > td {
				vertical-align: bottom;
			}
			.img-thumbnail {
				margin-bottom: 10px;
			}
		   </style>
			<table class="table table-striped table-bordered table-hover table-customfields">
				<tr>
					<th class="text-center">#</th>
					<th class="text-center">Тип поля</th>
					<th class="text-center">Имя поля</th>
					<th class="text-center">Описание поля</th>					
					<th class="text-center">Значение поля</th>
					<th></th>
				</tr>
		   <?php foreach ($fields as $field) { ?>
				<tr>
					
					<td class="text-center" style="vertical-align: middle;"><?php echo $i; ?></td>
					<td class="text-center">
						<select name="type-<?php echo $i; ?>" class="select-type form-control" >
							<?php foreach ($types as $type => $ttext) { ?>
								<option value="<?php echo $type; ?>" <?php if ($type == $field['type']) echo 'selected="selected"'; ?>><?php echo $ttext; ?></option>
							<?php } ?>
						</select>
					</td>
					<td class="text-center"><input type="text" name="name-<?php echo $i; ?>" value="<?php echo $field['name']; ?>" placeholder="Имя" class="form-control name-field" /></td>
					<td class="text-center"><input type="text" name="descr-<?php echo $i; ?>" value="<?php echo $field['description']; ?>" placeholder="Описание" class="form-control" /></td>
					<td class="text-center"><a href="" data-toggle="image" class="img-thumbnail <?php if ('image' !== $field['type']) echo 'hide'; ?>" id="thumb-image-<?php echo $i; ?>"><?php if ('image' == $field['type']) { ?><img src="/image/<?php echo $field['value']; ?>" alt="" title="" data-placeholder="Выбрать картинку" /><?php } ?></a><input type="text" name="value-<?php echo $i; ?>" value="<?php echo $field['value']; ?>" placeholder="Значение"  id="input-image<?php echo $i; ?>" class="form-control" /></td>
					<td class="text-center"><button type="button" data-toggle="tooltip" title="" class="btn btn-danger del" data-original-title="Удалить"><i class="fa fa-minus-circle"></i></button></td>
				</tr>
		   <?php $i++; } ?>	
				<tr>
                    <td colspan="5"></td>
                    <td class="text-center"><button type="button" data-toggle="tooltip" title="" class="btn btn-primary add" data-original-title="Добавить строку"><i class="fa fa-plus-circle"></i></button></td>
                  </tr>
			</table>
		   </div>
        </form>
			</div>
    </div>
  </div>
</div>
<script>
	$('.select-type').on('change', function(e){
		if ($(this).val() == 'image' ) {
			$(this).parents('tr').find('.img-thumbnail').removeClass('hide').html('<img src="" alt="Клик для выбора" title="" data-placeholder="Выбрать картинку" />');
		} else {
			$(this).parents('tr').find('.img-thumbnail').addClass('hide').html('');
		}
	});
	$('.del').click(function(e){
		$(this).parents('tr').remove();
		e.preventDefault();
	});
	$('.add').click(function(e){
		var num = $('.table-customfields tr').eq(-2).find('td').eq(0).html();
		if (num === undefined) num=0;
		num = parseInt(num)+1;
		$(this).parents('tr').before('<tr><td class="text-center" style="vertical-align: middle;">'+num+'</td><td class="text-center"><select name="type-'+num+'" class="select-type form-control"><option value="text" selected="selected">Текст</option><option value="image">Картинка</option></select></td><td class="text-center"><input type="text" name="name-'+num+'" value="" placeholder="Имя" id="input-name" class="form-control"></td><td class="text-center"><input type="text" name="descr-'+num+'" value="" placeholder="Описание" id="input-descr" class="form-control"></td><td class="text-center"><a href="" data-toggle="image" class="img-thumbnail hide" id="thumb-image-1"></a><input type="text" name="value-'+num+'" value="" placeholder="Значение" id="input-image'+num+'" class="form-control"></td><td class="text-center"><button type="button" data-toggle="tooltip" title="" class="btn btn-danger del" data-original-title="Удалить"><i class="fa fa-minus-circle"></i></button></td></tr>');
		e.preventDefault();
		$('.select-type').on('change', function(e){
			if ($(this).val() == 'image' ) {
				$(this).parents('tr').find('.img-thumbnail').removeClass('hide').html('<img src="" alt="Клик для выбора" title="" data-placeholder="Выбрать картинку" />');
			} else {
				$(this).parents('tr').find('.img-thumbnail').addClass('hide').html('');
			}
		});
		$('.del').click(function(e){
			$(this).parents('tr').remove();
			e.preventDefault();
		});
	});
	$('#form-cfilds').submit(function(e){
		require_no = false;
		$('.name-field').each(function(y){
			if (!$(this).val()) {
				$(this).css({'border-color' : 'red'});
				require_no = true;
			}
		});
		if (require_no) return false;
	});
</script>
<?php echo $footer; ?>