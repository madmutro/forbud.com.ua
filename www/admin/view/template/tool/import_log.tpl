<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
    </div>
  </div>
  <div class="container-fluid">
    <div class="alert alert-info"><i class="fa fa-check-info"></i>Автор отчёта: Artem Pitov (pitov.pro) <br> Автор модуля: Usergio (https://opencartforum.com/profile/26794-usergio/)</div>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-body">
        <h4> <?php echo $text_log_error; ?> </h4>
        <p>
          <?php if (!empty($error_log)) { ?> <span><?php echo $error_filemtime; ?></span><br><br> <?php } ?>
          <textarea wrap="off" rows="15" readonly class="form-control"><?php echo $error_log; ?></textarea>
        </p>
        <br><br>
        <h4> <?php echo $text_log_report; ?> </h4>
        <p>
          <?php if (!empty($report_log)) { ?>  <span><?php echo $report_filemtime; ?></span><br><br> <?php } ?>
          <textarea wrap="off" rows="15" readonly class="form-control"><?php echo $report_log; ?></textarea>
        </p>
        <br><br>
        <h4> <?php echo $text_log_sos; ?> </h4>
        <p>
          <?php if (!empty($sos_log)) { ?>  <span><?php echo $sos_filemtime; ?></span><br><br> <?php } ?>
          <textarea wrap="off" rows="15" readonly class="form-control"><?php echo $sos_log; ?></textarea>
        </p>        
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>