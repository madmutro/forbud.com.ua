<script src="view/javascript/aios/js/jquery-ui/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="view/javascript/aios/js/modernizr.min.js"></script>
<!--right slidebar-->
<script src="view/javascript/aios/js/slidebars.min.js"></script>
<!--switchery-->
<script src="view/javascript/aios/js/switchery/switchery.min.js"></script>
<script src="view/javascript/aios/js/switchery/switchery-init.js"></script>
<!--jquery countTo-->
<script src="view/javascript/aios/js/jquery-countTo/jquery.countTo.js"  type="text/javascript"></script>
<!--common scripts for all pages-->
<script src="view/javascript/aios/js/scripts.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //countTo
        $('.timer').countTo();
    });
</script>