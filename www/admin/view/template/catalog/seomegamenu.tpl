<div id="navbar-collapse-1" class="navbar-collapse collapse yamm mega-menu">
    <ul class="nav navbar-nav">
        <!-- Classic list -->
        <li class="dropdown"><a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle">Mega Menu <b
                class=" fa fa-angle-down"></b></a>
            <ul class="dropdown-menu wide-full">
                <li>
                    <!-- Content container to add padding -->
                    <div class="yamm-content">
                        <div class="row">
                            <ul class="col-sm-2 list-unstyled">
                                <li>
                                    <p class="title"><?php echo $text_autogenerators; ?></p>
                                </li>
                                <li><a href="<?php echo $autogenerate_product; ?>"><?php echo $text_product; ?></a>
                                </li>
                                <li><a href="<?php echo $autogenerate_category; ?>"> <?php echo $text_category; ?></a>
                                </li>
                                <li><a href="<?php echo $autogenerate_manufacturer; ?>"> <?php echo $text_manufacturer; ?></a>
                                </li>
                                <li><a href="<?php echo $autogenerate_information; ?>"> <?php echo $text_information; ?> </a>
                                </li>
                                <li><a href="<?php echo $autogenerate_general; ?>"> <?php echo $text_general; ?> </a>
                                </li>
                            </ul>
                            <ul class="col-sm-2 list-unstyled">
                                <li>
                                    <p class="title"><?php echo $text_editor; ?></p>
                                </li>
                                <li><a href="<?php echo $customize_product; ?>"><?php echo $text_product; ?></a>
                                </li>
                                <li><a href="<?php echo $customize_category; ?>"> <?php echo $text_category; ?></a>
                                </li>
                                <li><a href="<?php echo $customize_manufacturer; ?>"> <?php echo $text_manufacturer; ?></a>
                                </li>
                                <li><a href="<?php echo $customize_information; ?>"> <?php echo $text_information; ?> </a>
                                </li>
                                <li><a href="<?php echo $customize_general; ?>"> <?php echo $text_general; ?> </a>
                                </li>
                            </ul>
                            <ul class="col-sm-2 list-unstyled">
                                 <li>
                                    <p class="title"><?php echo $text_report; ?></p>
                                </li>
                                <li><a href="<?php echo $report_completeseo; ?>"><?php echo $text_report; ?></a>
                                </li>
                            </ul>
                            <ul class="col-sm-2 list-unstyled">
                                <li>
                                    <p class="title"><?php echo $text_socialrichsnippets; ?></p>
                                </li>
                                  <li><a href="<?php echo $snippet_google; ?>"><i class="fa fa-google"></i><?php echo $text_google; ?></a></li>
           <li><a href="<?php echo $snippet_twitter; ?>"><i class="fa fa-twitter"></i><?php echo $text_twitter; ?></a></li>
           <li><a href="<?php echo $snippet_facebook; ?>"><i class="fa fa-facebook"></i><?php echo $text_facebook; ?></a></li>
          <li><a href="<?php echo $snippet_pinterest; ?>"><i class="fa fa-pinterest"></i><?php echo $text_pinterest; ?></a></li> 

                            </ul>
                            <ul class="col-sm-2 list-unstyled">
                                <li>
                                    <p class="title"><?php echo $text_more; ?></p>
                                </li>
    <li><a href="<?php echo $seo_settings; ?>"><i class="fa fa-gear"></i>Settings</a></li>
    <li><a href="<?php echo $sitemap; ?>"><i class="fa fa-sitemap"></i><?php echo $text_sitemap; ?></a></li>
    <li><a href="<?php echo $redirectmanager; ?>"><i class="fa fa-random"></i><?php echo $text_redirectmanager; ?></a></li>
     <li><a href="<?php echo $failedlinks; ?>"><i class="fa fa-remove"></i><?php echo $text_failedlinks; ?></a>
    </li>
     <li><a href="<?php echo $clearseo; ?>"><i class="fa fa-bomb"></i><?php echo $text_clearseo; ?></a>
    </li>
                            </ul>
                            <ul class="col-sm-2 list-unstyled custom-nav-img">
                                <li>
                                    <p class="title"><?php echo $text_aboutallinoneseo; ?></p>
                                </li>
                                <li>
                                    <p class="desk">
                                        <?php echo $text_description; ?>
                                    </p>
                                </li>
                            </ul>

                        </div>
                    </div>
                </li>
            </ul>
        </li>

    </ul>
</div>